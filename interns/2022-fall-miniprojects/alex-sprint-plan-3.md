## Project Description
**Project:** "Django-Delvin"

**Implementer:** Alex

**Project Description:**

Delvin is an analytics platform that ingests message data from chatbots to help nonprofit organizations analyze the activity happening in their chatbot.  We have incorporated several platforms into the project, including Landbot, TextIt, and Turn.io.  We would like to incorporate more platforms, including Telegram.


## Current Sprint

NA

## Past Sprints

### **Sprint 3**

**Sprint Goal**

**Goal 1:** We are working with a platform called Turn.io to develop chatbots.  The platform is difficult to use given it's in an alpha state.  We would like to refine a script generation prototype to make our work faster, automated, and more manageable.

**Goal 2:** We would like to be able to save the data from all platforms based on our [model schema](https://gitlab.com/tangibleai/django-delvin/-/blob/feature-pulling-chatbot-message/message/models.py).

**Current Sprint Tasks:**
The Elixir files are the source of truth for the output we want.  These were built manually and run.  The Excel files and code generator need to build something like the Elixir file (just a .txt file is fine, but the structure should be the same).  Try to work through the code to get the output of the Excel/code generator to match the two Elixir files.

- [x] A 1-0.5 : Review the Jupyter notebook with the code generator.  Pay particular attention to the card template types.
  * Download the Project. Prepare the environment
- [x] A 0.5-0.5 : Review the "Active Listening" chatbot template to understand what the code generator is ingesting.  Note the card types in Column A and how they function within the conversation.
- [x] A 2-1 : Convert the Jupyter notebook into a standalone script that runs with the following command: python ./turn_script_generator.py {script_file_name.xlxs}
  * added debug mode
  * Fill in the "main menu" script to look like the "active listening" script (ie., column headings, Column A template type labels, etc.).  The goal here is to understand all the parts of the script and how the code generator is ingesting it.
- [x] : Review the Intro and Active Listening scripts for any new card types.  Make new card templates for any card type that doesn't already have a template.  For example, the following could be a "Card - New Stack" type.
  ```
  card Row{row_num} do
    schedule_stack({UUID}, in: {seconds})
  end
  ```

  - Wrap each card template in a function that will build that template through an f"" and return the prepared card.
  - Update the Excel template when necessary so that the code generator can correctly use the new template cards.  For example, you may want to add a column to the Excel for "New stack" that takes the stack UUID.
- [x] A 2-4 Update Excel (Aktive listening) + Script
  * Debug
- [x] A 2-2 Update Excel (Intro) + Script
  * Debug
  -- Multiple mainmenu

- [x] A - : **Medium priority** Add documentation to the `/docs` folder for both Telegram.  The docs should include:
  * step-by-step instructions for making a bot and setting up any messagehook, webhook, api required
  * a sample output object from a messagehook, webhook, api





### **Sprint 2**

**Sprint Goal**

**Goal 1:** Now that we have the ability to receive data from Turn.io, Landbot, and (next task) Telegram.  We would like to be able to save the data based on our [model schema](https://gitlab.com/tangibleai/django-delvin/-/blob/feature-pulling-chatbot-message/message/models.py).

**Goal 2:** Delvin needs to work with many platforms to meet users where they are at.  We would like Delvin to be able to receive message data from a Telegram chatbot/chatroom and, ultimately, store it in a database.

**Current Sprint Tasks:**
- [x] A 2-2.5: Issue 2 (high priority - by early next week is best)
  * Make sure that the Landbot integration is working, and that you can receive the data object from the messagehook.
  * Review the [Landbot Message Hook documentation](https://gitlab.com/tangibleai/django-delvin/-/blob/main/docs/landbot-webhook.md) to understand the context of this function.
  * Use this Landbot chatbot to test your fix.  [Sample Landbot](https://landbot.pro/v3/H-1421823-ZGLC9V27V1SPTS1B/index.html)


- [x] A 0.5-0.5 : Understand the data schema we want all the integrations to follow
  * Review the model fields we need from [models.py](https://gitlab.com/tangibleai/django-delvin/-/blob/feature-pulling-chatbot-message/message/models.py)
    - Project = Chatbot project's name
    - Contact = user
    - Message = Chatbot message interaction content

- [x] A 1-2.5 : Each integration should have data objects for all 3 models (ie., project_data, contact_data, message_data)
  * Landbot
  * Turn.io
  * Telegram

- [x] A 2-?: Create (or reuse) a simple Telegram bot
  * Make a simple Telegram bot with a few exchanges

- [x] A 1-1: Make Django-Delvin collect the relevant data from a Telegram message
  * You might find this [quick tutorial](https://betterprogramming.pub/how-to-get-data-from-telegram-82af55268a4b) helpful
  * Django-Delvin should be able to receive data from the Telegram chatbot.
  * Filter the data object from Telegram to get the information we need in a JSON object/dictionary







### **Sprint 1**

**Sprint Goal**

There are two issues with platform intergrations that need to be fixed in the Django-delvin application.

**Completed Sprint Tasks**
- Preparation Steps
  * [x] Set up an account on Gitlab
  * [x] Send Greg your Gitlab id.
  * [x] Set up an account on [Render.com](https://render.com/)
  * [x] Review the [Django Project Process document](https://gitlab.com/tangibleai/team/-/blob/main/exercises/resources/django-project-processes.md) to understand the project architectures and processes we are working towards
  * [x] Give Alex access to the [Django-Delvin repository](https://gitlab.com/tangibleai/django-delvin) (after getting his Gitlab id).
  * [x] Clone the project, and work on the [feature-platform-connections branch](https://gitlab.com/tangibleai/django-delvin/-/tree/feature-platform-connections).
  
- Issue 1

  * On line 130, there is a problem accessing the 'messages' content in the `data_json` object we get from Turn.io.  While the integration still seems to be processing requests correctly, we don't want to receive this error message.
    ```
    File "/opt/render/project/src/maitag/platforms/turn.py", line 130, in label_and_resubmit_message
    Nov 17 01:41:41 PM      message = data_json['messages'][0]['text']['body']
    Nov 17 01:41:41 PM  KeyError: 'messages'
    ```
  * [x] Review the `intent-recognition-overview.md` in `django-delvin/docs`:  [link](https://gitlab.com/tangibleai/django-delvin/-/blob/main/docs/intent-recognition-overview.md) to understand the context of this file and function.  Consider the "Request Object" section in particular.
  * [x] Update the code on line 130 (or any other necessary code that is contributing to that error) to make sure the code runs without an error message.

**Remaining Sprint Tasks**
- Issue 2
  * [ ] Make a [Landbot account](https://landbot.io/)
  * [ ] The Landbot integration is throwing a [500 error](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#5xx_server_errors).  Diagnose the cause of this error and try to correct it.
      ```
      Nov 17 01:54:19 PM  10.204.127.219 - - [17/Nov/2022:04:54:19 +0000] "POST /api/turn/label/ HTTP/1.1" 500 96025 "-" "Turn/4.282.1"
      ```
  * [ ] Review the [Landbot Message Hook documentation](https://gitlab.com/tangibleai/django-delvin/-/blob/main/docs/landbot-webhook.md) to understand the context of this function.
  * [ ] Use this Landbot chatbot to test your fix.  [Sample Landbot](https://landbot.pro/v3/H-1421823-ZGLC9V27V1SPTS1B/index.html)
    - This Landbot is hooked up to the site, so you don't need to follow the directions to make your own (unless you want to).
    - The Landbot is limited to 100 conversations per month.  If you leave the tab open and continue chatting with it as you work, it will only count as 1 conversation.  Closing the conversation and visiting the link again will count as a separate conversation.
