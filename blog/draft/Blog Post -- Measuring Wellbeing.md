# Elevate Prosocial AI

How might we do this? Psychology, data, statistics and a bit of machine learning can be our friends here.

My friend Jeffery Martin faced a problem of measuring the degree of "well being" experienced by various people. He wanted to measure whether his Finders Course consciousness-expansion program was actually helping people improve their lives. What he came up with was a battery of already-existing psychological measures, such as:

    Authentic Happiness Inventory

    Center for Epidemiology Studies-Depression Scale

    PERMA Profiler Questionnaire (Positive Emotion, Engagement, Relationships, Meaning, and Accomplishment

    Satisfaction with Life Scale

    Gratitude Questionnaire

    Fordyce Emotions Questionnaire

    Meaning in Life Questionnaire

    State-Trait Anxiety Inventory

    Perceived Stress Scale

(In his study Jeffery also measured a Mysticism Scale and Perceived Nondual Embodiment Thematic Inventory, which measure "spiritual" state rather than generic well-being, but these are less relevant to the topic of the current blog post, though also certainly interesting to measure.)

Each of these survey instruments has its own issues and complexities, and each also has a body of serious research literature behind it. The point is not that these measures give us a perfect way to measure human well-being, but rather that they give us a decent stab at it, which is definitely worth taking.
