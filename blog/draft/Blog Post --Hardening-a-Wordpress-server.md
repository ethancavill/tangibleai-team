# Hardening Wordpress (LightSpeed) web server

XML-RPC is a legacy protocol from before Wordpress was even called wordpress.
It allows admins to upload XML content over RPC (usually with a mobile app).

Soon after Defcon 2023: A random cybersecurity researcher sent us a machine generated "ethical" bug report and requested a bounty.

Our server shows that XML-RPC is still enabled on our server because [tangibleai.com/xmlrpc.php](https://tangibleai.com/xmlrpc.php) returns "<HTML>XML-RPC server accepts POST requests only.</HTML>".
I did not try to exploit it or even send it a valid POST request.

The new REST-API makes this unnecessary unless you're using Jetpack or some legacy mobile app for submitting content over XML-RPC.
And the XML-RPC may allow a hacker to exploit vunlerabilities that give them access to the server and the VPC on Digital Ocean.

### References

* [LiteSpeed Wordpress tutorial](https://cleanleaf.com/docs/admin.html)
* [How to disable XML-RPC in Wordpress](https://www.wpbeginner.com/plugins/how-to-disable-xml-rpc-in-wordpress/) on wpbeginner.com (FAIL)
* [AIOS compatibility with Open Litespeed](https://wordpress.org/support/topic/aios-compatibility-with-litespeed-openlitespeed/) - uses rewrite rules (SUCCESS?)
* [Intro to .htaccess rewrite rules](https://acquia.my.site.com/s/article/360005257234-Introduction-to-htaccess-rewrite-rules) - customized my own rewrite rules (FAIL)

Tried to disable it with a Files filter/firewall, but that didn't work (nothing changed):

```bash
cat << EOF >> /var/www/html/.htaccess

##########################################
# 2023-08-25 hardening by Hobs

# # WARNING: Litespeed servers ignore all .htaccess rules except rewrites
# # Trying to mitigate bug report e-mail from gray hat hacker Harris A
# # SEE: https://www.hostinger.com/tutorials/xmlrpc-wordpress
<Files xmlrpc.php>
order deny,allow
deny from all
# allow from xxx.xxx.xxx.xxx
</Files>

# 2023-08-25 hardening by Hobs
##########################################
EOF
```

### My custom rules

```bash
cat << EOF > /var/www/html/.htaccess.header

### Forcing HTTPS rule start       
RewriteEngine On
RewriteCond %{HTTPS} off

######################################################
# 2023-08-24 Hobs hardening (disable xmlrpc.php)
# References:
#   * https://wordpress.org/support/topic/aios-compatibility-with-litespeed-openlitespeed/
#   * https://acquia.my.site.com/s/article/360005257234-Introduction-to-htaccess-rewrite-rules
RewriteRule xmlrpc.php https://%{HTTP_HOST}/404.html [R=404,L]
# 2023-08-24 Hobs hardening
######################################################

RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]
### Forcing HTTPS rule end

EOF

# replace first 5 lines with the heredoc above
sed -i '1,5{R /var/www/html/.htaccess.header
        d}' /var/www/html/.htaccess
```

## Web server commands

### Forensics

#### `lastb`
Find out who recently logged into the server

#### IP Addr

```bash
IPADDR=$(curl ifconfig.me) echo $IPADDR
echo $(curl -4/-6 icanhazip.com)
echo $(curl ipinfo.io/ip)
echo $(curl api.ipify.org)
echo $(curl checkip.dyndns.org)
echo $(dig +short myip.opendns.com @resolver1.opendns)
```

#### Hostname
```bash
hostname
echo $HOSTNAME
```

```bash
nslookup $IPADDR

112.214.230.157.in-addr.arpa    name = openlitespeedwordpress601onubuntu2204-s-1vcpu-1gb-intel-nyc1-01.
112.214.230.157.in-addr.arpa    name = openlitespeedwordpress601onubuntu2204-s-1vcpu-1gb-intel-nyc1-01.local.
```

None of these find the tangibleai.com DN.

```bash
host -C $IPADDR
host $IPADDR
host $(host $IPADDR)
host -a "$(host $IPADDR)"
host -C "$(host $IPADDR)"
```

```bash
dig -x $IPADDR

; <<>> DiG 9.18.1-1ubuntu1.2-Ubuntu <<>> -x 157.230.214.112
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 31209
;; flags: qr aa rd ra ad; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;112.214.230.157.in-addr.arpa.  IN      PTR

;; ANSWER SECTION:
112.214.230.157.in-addr.arpa. 0 IN      PTR     openlitespeedwordpress601onubuntu2204-s-1vcpu-1gb-intel-nyc1-01.
112.214.230.157.in-addr.arpa. 0 IN      PTR     openlitespeedwordpress601onubuntu2204-s-1vcpu-1gb-intel-nyc1-01.local.

;; Query time: 4 msec
;; SERVER: 127.0.0.53#53(127.0.0.53) (UDP)
;; WHEN: Fri Aug 25 22:59:56 UTC 2023
;; MSG SIZE  rcvd: 217
```

#### xmlrpc.php
```bash
curl https://tangibleai.com/xmlrpc.php
XML-RPC server accepts POST requests only.
```

```bash
curl http://$(hostname)/xmlrpc.php || curl http://tangibleai.com/xmlrpc.php

<!DOCTYPE html>
<html style="height:100%">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title> 404 Not Found
</title></head>
<body style="color: #444; margin:0;font: normal 14px/20px Arial, Helvetica, sans-serif; height:100%; background-color: #fff;">
<div style="height:auto; min-height:100%; ">     <div style="text-align: center; width:800px; margin-left: -400px; position:absolute; top: 30%; left:50%;">
        <h1 style="margin:0; font-size:150px; line-height:150px; font-weight:bold;">404</h1>
<h2 style="margin-top:20px;font-size: 30px;">Not Found
</h2>
<p>The resource requested could not be found on this server!</p>
</div></div><div style="color:#f0f0f0; font-size:12px;margin:auto;padding:0px 30px 0px 30px;position:relative;clear:both;height:100px;margin-top:-101px;background-color:#474747;border-top: 1px solid rgba(0,0,0,0.15);box-shadow: 0 1px 0 rgba(255, 255, 255, 0.3) inset;">
<br>Proudly powered by  <a style="color:#fff;" href="http://www.litespeedtech.com/error-page">LiteSpeed Web Server</a><p>Please be advised that LiteSpeed Technologies Inc. is not a web hosting company and, as such, has no control over content found on this site.</p></div></body></html>
```

#### LiteSpeed wordpress server

```bash
export PATH=/usr/local/lsws/bin:$PATH
lswsctrl status
lswsctrl stop
/usr/local/lsws/bin/lswsctrl start
/usr/local/lsws/bin/lswsctrl restart
/usr/local/lsws/bin/lswsctrl reload
```

```bash
export PATH=/usr/local/lsws/bin:$PATH
litespeed --version

LiteSpeed/1.7.16 Open (BUILD built: Thu Nov 17 16:18:46 UTC 2022) 
        module versions:
        lsquic 3.1.1
        modgzip 1.1
        cache 1.64
        mod_security 1.4
```

```bash
cat << EOF > /var/www/html/.htaccess.insertme
######################################################
# 2023-08-24 Hobs hardening (disable xmlrpc.php)
# References:
#   * https://wordpress.org/support/topic/aios-compatibility-with-litespeed-openlitespeed/
#   * https://acquia.my.site.com/s/article/360005257234-Introduction-to-htaccess-rewrite-rules
RewriteRule xmlrpc.php https://%{HTTP_HOST}/404.html [R=404,L]
# 2023-08-24 Hobs hardening
######################################################

EOF
```

This single RewriteRule line needs to inserted on line 11 right after
```bash
<IfModule LiteSpeed>
RewriteEngine on
```

A stack overflow multiline sed might work. This example supposedly replaces 'why\nhuh\n' with 'yo\n'

```bash
sed 'N; s/\<'"$LINE1"'\>\n\<'"$LINE2"'\>/'"$NEWLINES"'/g' test.txt
```

If the variables are defined like this.

```bash
$LINE1='<IfModule LiteSpeed>'
$LINE2='RewriteEngine On'  # Litespeed may miscapitalize this as "on" instead of "On"

NEWLINES='
'"$LINE1"'
'"$LINE2"
RewriteRule xmlrpc.php https://%{HTTP_HOST}/404.html [R=404,L]
'
```

So grep for these lines, or just `nano` the file so that it looks like this:

```bash
head -n 10 /var/www/html/.htaccess

### Forcing HTTPS rule start       
RewriteEngine On
RewriteCond %{HTTPS} off
RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]
### Forcing HTTPS rule end

# BEGIN LSCACHE
## LITESPEED WP CACHE PLUGIN - Do not edit the contents of this block! ##
<IfModule LiteSpeed>
RewriteEngine On
######################################################
# 2023-08-24 Hobs hardening (disable xmlrpc.php)
# References:
#   * https://wordpress.org/support/topic/aios-compatibility-with-litespeed-openlitespeed/
#   * https://acquia.my.site.com/s/article/360005257234-Introduction-to-htaccess-rewrite-rules
# 2023-08-24 Hobs hardening
######################################################
RewriteRule xmlrpc.php https://%{HTTP_HOST}/404.html [R=404,L]
```
