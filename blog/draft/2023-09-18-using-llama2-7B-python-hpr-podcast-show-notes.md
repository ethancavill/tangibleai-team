# Using Hugging Face LLMs like Llama2 in Python

We're developing an open source AI assistant you can trust.
We're calling it Qary for now, Q, A, R, Y -- Q as in question, A as in answer, r, y.

1. Create a Hugging Face account:
  - [huggingface.co/join](https://huggingface.co/join)

2. Create and copy your access token:
  - [Your user profile](https://huggingface.co/settings/tokens)

3. Create a .env file with your access token string:
```bash 
echo "HUGGINGFACE_ACCESS_TOKEN=hf_..." >> .env
```

4. Load the `.env` variables in your python script using `dotenv` package and os.environ:
- TIP: Use `os.environ` to retrieve the dict of variable values rather than `dotenv.load_values`- Otherwise other environment variables that have been set by other shell scripts such as `.bashrc` will be ignored.
- This confused us when we were getting our GitLab CI-CD pipeline working and deploying to Render.com.
- Each of your cloud services will have different approaches to setting environment variables.
- This token string can be passed as a keyword argument to most of the pipeline and model classes.
```python
import dotenv
dotenv.load_dotenv()
import os
env = dict(os.environ)
auth_token = env['HUGGINGFACE_ACCESS_TOKEN']
```

5. Find the path and name for the model on Hugging Face hub you want to use:
- search for "llama2" in the top search bar on [huggingface.co/](https://huggingface.co/)
- TIP: don't hit enter at the end of your search, instead click on "See 3958 model results for llama2" 
- Click on [meta-llama/Llama-2-7b-chat-hf](https://huggingface.co/meta-llama/Llama-2-7b-chat-hf) to see the documentation

6. Apply for a license to get access to the model
- Llama2 is not truly open source, it's "business source" licensed by Facebook
- Apply for a license to use Llama2 on [ai.meta.com](https://ai.meta.com/resources/models-and-libraries/llama-downloads/) using the same e-mail you used for your Hugging Face account.
- Don't use Llama2 if you think you'll have more than a million users, because that would violate the terms of the licenes (that no one reads).

7. Follow the [instructions on huggingface.co](https://huggingface.co/docs/hub/security-tokens) to authenticate your python session
- For an `AutoModel` class you would use the `use_auth_token` in the `.from_pretrained` method.
- In the LlamaTokenizer `` 
- The `use_auth_token` kwarg should be set to the personal access token you copied from your Hugging Face profile page and pasted in the `.env` file.
- The Hugging Face docs say to use the `token` kwarg, but that never worked for me.

```python
>>> from transformers import LlamaForCausalLM, LlamaTokenizer
>>> model_name = "meta-llama/Llama-2-7b-chat-hf"
>>> auth_token = "YOUR_HF_ACCESS_TOKEN_HERE"  # <1>
>>> llama = LlamaForCausalLM.from_pretrained(
...     model_name,  # <2>
...     token=auth_token)
>>> tokenizer = LlamaTokenizer.from_pretrained(
...     model_name,
...     token=auth_token)
>>> prompt = "Q: How do you know when you misunderstand the real world?\n"
>>> prompt += "A: "  # <2>
>>> input_ids = tokenizer(prompt, return_tensors="pt").input_ids
>>> input_ids  # <3> 
tensor([[    1,   660, 29901, ...  13, 29909, 29901, 29871]])
```
<1> Follow the instructions on Hugging Face to retrieve your personal access token
<2> The model will be downloaded from Hugging Face Hub at this path if it does not already exist on your hard drive
<3> The smaller LLMs often work better if nudge them with explicit Question/Answer prompts.
<4> The PyTorch model can only process batches of tensors

This should output a batch of encoded prompts, rather than just a single prompt.
This is why you see a 2-D tensor in the output, but it only has a single row for the one prompt you just encoded.
If you prefer you can process multiple prompts in parallel by running the tokenizer on a list of strings rather than a single string.

The llama2 model requires 10 GB of storage, and once they are decompressed into a working model in RAM they will likely use as much as 34 GB.
This code the model weights from Hugging Face Hub which took more than 5 minutes on our 5G Internet connection.
So make sure you have something else to do when you run this code for the first time.
And even if the model has already been downloaded and saved in your environment, it may take a minute or two just to load the model into RAM.
And generating the response to your prompt may also require a couple minutes as it does the 7 billion multiplications required for each token in the generated sequence.

```
>>> max_answer_length = 30
>>> output_ids = llama.generate(
...     input_ids,
...     max_length=100)  # <1>
>>> tokenizer.batch_decode(output_ids)[0]
```

You can also generate the tokens one at a time to make it feel more interactive for your users.

```python
>>> prompt = "Q: How do you know when you misunderstand the real world?\nA:"
>>> input_ids = tokenizer(prompt, return_tensors="pt").input_ids
>>> input_ids

>>> print(prompt, end='', flush=True)
>>> while not prompt.endswith('</s>'):
...     input_ids = tokenizer(prompt, return_tensors="pt").input_ids
...     input_len = len(input_ids[0])
...     output_ids = llama.generate(
...         input_ids, max_length=input_len + 1)
...     ans_ids = output_ids[0][input_len:]
...     output_str = tokenizer.batch_decode(
...         output_ids, skip_special_tokens=False)[0]
...     output_str = output_str[3:]  # <1>
...     tok = output_str[len(prompt):]
...     print(tok, end='', flush=True)
...     prompt = output_str
```
<1> The output str will start with the special start token ("<s>") unless you remove it.


# FIXME:
```
<>:1: SyntaxWarning: "is not" with a literal. Did you mean "!="?
<>:1: SyntaxWarning: "is not" with a literal. Did you mean "!="?
<ipython-input-15-d63fc4b81149>:1: SyntaxWarning: "is not" with a literal. Did you mean "!="?
  while tok.strip() is not '</s>':
<ipython-input-15-d63fc4b81149>:1: SyntaxWarning: "is not" with a literal. Did you mean "!="?
  while tok.strip() is not '</s>':
Input length of input_ids is 19, but `max_length` is set to 2. This can lead to unexpected behavior. You should consider increasing `max_new_tokens`.
<s>Q: How do you know when you misunderstand the real world?
A:  When
A:  When

Input length of input_ids is 22, but `max_length` is set to 2. This can lead to unexpected behavior. You should consider increasing `max_new_tokens`.
<s>Q: How do you know when you misunderstand the real world?
A: A:  When your
hen your

Input length of input_ids is 24, but `max_length` is set to 2. This can lead to unexpected behavior. You should consider increasing `max_new_tokens`.
<s>Q: How do you know when you misunderstand the real world?
A: A:  Whenhen your understanding
our understanding

Input length of input_ids is 26, but `max_length` is set to 2. This can lead to unexpected behavior. You should consider increasing `max_new_tokens`.
<s>Q: How do you know when you misunderstand the real world?
A: A:  Whenhen yourour understanding of
ing of

Input length of input_ids is 28, but `max_length` is set to 2. This can lead to unexpected behavior. You should consider increasing `max_new_tokens`.
<s>Q: How do you know when you misunderstand the real world?
A: A:  Whenhen yourour understandinging of the
 of the

Input length of input_ids is 30, but `max_length` is set to 2. This can lead to unexpected behavior. You should consider increasing `max_new_tokens`.
<s>Q: How do you know when you misunderstand the real world?
A: A:  Whenhen yourour understandinging of of the real
the real

Input length of input_ids is 32, but `max_length` is set to 2. This can lead to unexpected behavior. You should consider increasing `max_new_tokens`.
<s>Q: How do you know when you misunderstand the real world?
A: A:  Whenhen yourour understandinging of of thethe real world
eal world

Input length of input_ids is 35, but `max_length` is set to 2. This can lead to unexpected behavior. You should consider increasing `max_new_tokens`.
<s>Q: How do you know when you misunderstand the real world?
A: A:  Whenhen yourour understandinging of of thethe realeal world is
rld is

Input length of input_ids is 38, but `max_length` is set to 2. This can lead to unexpected behavior. You should consider increasing `max_new_tokens`.
<s>Q: How do you know when you misunderstand the real world?
A: A:  Whenhen yourour understandinging of of thethe realeal worldrld is not
 is not

Input length of input_ids is 40, but `max_length` is set to 2. This can lead to unexpected behavior. You should consider increasing `max_new_tokens`.
^C<ipython-input-15-d63fc4b81149>:1: SyntaxWarning: "is not" with a literal. Did you mean "!="?
  while tok.strip() is not '</s>':
<ipython-input-15-d63fc4b81149>:1: SyntaxWarning: "is not" with a literal. Did you mean "!="?
  while tok.strip() is not '</s>':
<ipython-input-15-d63fc4b81149>:1: SyntaxWarning: "is not" with a literal. Did you mean "!="?
  while tok.strip() is not '</s>':
<ipython-input-15-d63fc4b81149>:1: SyntaxWarning: "is not" with a literal. Did you mean "!="?
  while tok.strip() is not '</s>':
```

## A more reasonable test of reasoning ability

### Test it
Whenever you take an exam the teacher is careful not to ask you questions that you've already seen.
With LLMs that's hard to do.
They have read more tests and textbooks than you would ever be able to read in many many lifetimes.
But there's hope.
You can use the N-Gram viewer to find all the ways that humans string words together.
So make sure the important phrases you use in your question are weird enough that an LLM will need to _generalize_ from the other things it has read to understand what you are asking.

If you want it to give you smart-__sounding__ answer you'll need to craft a prompt with common phrases that the LLM has seen in many different contexts before.
But if you want to see how smart the LLM really is, you need to get creative and ask it questions in a unique way.
Your wording needs to be different than anything you can imagine ever seeing on social media, where LLMs get most of their data. 

For example, consider this word problem prompt: 

"A farmer has 1 cow & 2 bulls. A cow lost its leg to sepsis, and a bull lost his leg in a rodeo accident. How many legs of cattle does he have?"

Notice how the prompt uses the phrase "legs of cattle" and also uses pronouns a lot rather than referring directly to objects in the problem.
You'd expect an LLM to recognize the sequence "How many head of cattle," which probably existis in hundreds or thousands of different texts that the LLM has been trained on.'
Head of cattle is a common unit of measure among ranchers and cowboys.
But this question changes that concept around when it asks "How many legs of cattle does he have?"
It's not asking the LLM to count the cows, but rather a piece of their anatomy.
An LLM should have seen questions and answers about the number of legs that a cow has.
But LLMs probably have never seen this particular word problem before.
This question should break an LLM's pattern-matching and memorization/regurgitation approach to simulating reasoning.

You can check Google n-gram-viewer for the number of times a phrase has appeared in books and articles over the past few years.
You should chose a phrases with zero frequency in the N-gram database whenever you want to test an LLM's intelligence and not its ability to memorize.
Here are the n-gram frequencies we found in the database for these phrases.

```python
>>> pd.DataFrame([
...   ["many legs of cattle",  0   ],
...   ["many leg of cattle",   0   ],
...   ["many heads of cattle", 1e-9],
...   ["many head of cattle",  6e-9],
...   ], 
...   columns=['n_gram', 'frequency'])

                 n_gram  frequency
0   many legs of cattle      0e+00
1    many leg of cattle      0e+00
2  many heads of cattle      1e-09
3   many head of cattle      6e-09
```

So here's the prompt we used to see if LLama2 or GPT-3.5 are able to reason about things that a 3 year old should be able to handle.


```python
>>> prompt = "A farmer has 2 cows & 2 bulls."
>>> prompt += "One cow lost its leg to sepsis, "
>>> prompt += "and a bull lost a leg in a rodeo accident. "
>>> prompt += "How many legs of cattle does he have?"
```

## What about ChatGPT?

Using the only publicly available version (3.5), ChatGPT doesn't do much better than the Llama2 open source model.
But it's definitely more talkative and confident.

### Ask it directly about common sense

#### Question
```
Are you able to use common sense and logic to reason about the world and physical things?
```

#### GPT-3.5
```
I can certainly use common sense and logic to reason about the world and physical things to some extent. My responses are generated based on patterns and information present in the text data I was trained on up to my knowledge cutoff date in September 2021. I can provide information, answer questions, and engage in logical reasoning within the scope of the knowledge and context available in that data.

However, it's important to note that my understanding of the world is based on textual data and patterns in language, and I do not have real-time access to the internet or the ability to observe the physical world directly. My responses are only as accurate and up-to-date as the information in my training data, and I may not have information on events or developments that occurred after my knowledge cutoff date.

While I can assist with a wide range of topics and provide logical responses based on my training, I may not always have the most current or complete information about specific events or emerging situations. Therefore, it's essential to verify information from reliable sources and use critical thinking when interpreting any responses I provide.
```



#### Prompt
```
A farmer has 2 cows & 2 bulls. One cow lost its leg to sepsis, and a bull lost a leg in a rodeo accident. How many legs of cattle does he have?
```

#### ChatGPT
```
Cattle typically have four legs each. So, if the farmer has 2 cows, they have a total of 2 cows * 4 legs/cow = 8 legs.

And if the farmer has 2 bulls, they also have a total of 2 bulls * 4 legs/bull = 8 legs.

Now, even though one cow lost a leg due to sepsis and one bull lost a leg in a rodeo accident, you would still count them as having 4 legs each. So, the total number of legs for the cattle remains the same:

8 legs (from the cows) + 8 legs (from the bulls) = 16 legs

So, the farmer has 16 legs of cattle.
```

### Second attempt at leg counting

#### Prompt
```
A farmer has 2 cows & 2 bulls. One cow lost its leg to sepsis, and a bull lost a leg in a rodeo accident. How many total working legs does his cattle have?
```
#### ChatGPT
```
Cattle typically have four working legs each. However, since one cow lost a leg to sepsis and one bull lost a leg in a rodeo accident, we need to account for these losses.

So, let's calculate:

    2 cows * 4 working legs/cow = 8 working legs (for the two cows)
    2 bulls * 4 working legs/bull = 8 working legs (for the two bulls)
    Subtract the lost legs: 1 leg from the cow + 1 leg from the bull = 2 lost legs

Now, subtract the 2 lost legs from the total working legs:

8 working legs (cows) + 8 working legs (bulls) - 2 lost legs = 14 working legs

So, the farmer's cattle have a total of 14 working legs.
```

## More detail

- [Raw terminal session (.txt)](./2023-09-18-using-llama2-7B-python-hpr-terminal-session-raw.txt)
- [Terminal session inputs and outputs (.yaml)](./2023-09-18-using-llama2-7B-python-hpr-terminal-session-input-output.yaml)
