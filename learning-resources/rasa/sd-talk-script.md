# San Diego Monthly Meetup 

## Lightning talk: Rasa dialog trees without the trees

Presenter: Rochdi Khalid

## Talk script

### Slide 1: Rasa Dialog Trees without the Trees

Rasa is an open-source chatbot framework to create and automate conversational applications. It provides several features to help you efficiently develop your bot. 

### Slide 2: How does Rasa work?

Rasa framework contains two libraries, **Rasa Core** and **Rasa NLU**. 

**Rasa NLU** performs the natural language understanding of the framework, It's the intrepreter that interprets and classifies intents. While **Rasa Core** manages the conversation state to execute the action needed and send a message to the user. 

The story begins when the user sends an input to the bot. Here, **Rasa NLU** will interpret the input to classify intents and extract other particular information. 

Next, **Rasa Core** comes to manage and maintain the conversation state using the following parameters:

* **Tracker** is used to keep track of the conversation history and save it in memory.

* **Policy** is the decision-maker of Rasa, it decides on what action the bot should take at every step in the conversation.

* **Featurizer** with the help of tracker, it generates the vector representation of the current conversation state so the action can pass later a tracker instance after every interaction with the user.


### Slide 3: Finite State Machine (FSM)

Even though Rasa can participate in a dialog, it just doesn’t use dialog trees. Here, Rasa depends on one of the most interesting concepts in computer science called FSM (Finite State Machine):

* It is a computational model that consists of one or more states.
* It can be only in a single active state at any given time.
* An input to the machine causes a transition from the current state to the next state.
* Each state defines the rules that determine which state to switch to based on the inputs to the machine.*

Let’s take an example of a light bulb. This object has two states, it’s either turned on or turned off. If we turn on the bulb, this can cause a transition from turned off to turned on and vice versa. ON defines the state to switch to which is OFF, and OFF defines the state to switch to which is ON. In other words, if the current state is ON, clicking on ON changes nothing, but pushing OFF changes the state to OFF. If we are in state OFF, pressing OFF keeps the machine in the same state, but pushing ON makes a transition to state ON.

### Slide 4: NLU and Intent Classification

As known, the most essential part of the process of building a bot is the data on which your bot will be based on it to learn how to recognize the user inputs. Rasa comes with different types of training data (top-level keys) to train your bot, they vary in terms of structure and functionality.

**NLU** can define the training data and the example utterances to categorize what the user wants to achieve during an interaction with a bot.

Rasa gives you the freedom to define the **NLU** training data by classifying the user utterances into categories that represent intents. Those components are the skeleton of **NLU** to describe what users might say in each category, you can give them any name you want but it is preferable for each intent to have a name that matches the goal users like to accomplish with that intent.

### Slide 5: Conversation Flow

Then, the conversation flow between the user and the bot is managed by another top-level key called **stories**. Here, the user inputs are defined as intents which are just the identifiers you defined in the training data. The bot responses are defined as **actions** where you can automate the bot to respond to the user inputs based on intents and stories.

The stories create the design of the conversation that determines the steps of the entire interaction.

(In other words, stories define the dialog tree. The dialog tree is actually a graph consists of connections between bot states. A FSM and a dialog tree are just a special kind of graph. Each state is something the bot will do or say. The edges or transitions between states are the actions or statements the user makes).

### Slide 6: Entity Recognition

Another usage of **NLU** is to identify and extract the structured information from user inputs. Here, we are talking about **Entities**. The term **ENTITIES** belongs to a big area of research in NLP named **NER** which stands for **N**amed **E**ntity **R**ecognition.

In Rasa, **entities** are structured pieces of information that can be extracted from user messages, it can hold important detail about the user like numbers, dates, names so that the bot could use them later in the conversation. 

Let’s take an example for a flight booking, it would be very useful for your bot to know which detail in user input refers to a destination.

In Rasa, entities can be extracted in many ways whether using synonyms, lookup tables, or even regular expressions. 

### Slide 7: Rasa Policies

**Policies** in Rasa are the decision-makers in every step of a conversation, each policy predicts the action with a particular confidence level. 

A policy with a high level of confidence is the one that decides on the action needed for the next step of a conversation.

In Rasa, there are different **machine learning policies** and each policy has its own way to make a decision:


* **TED policy** is a transform embedding architecture that performs next action prediction and entity recognition tasks based on transformer encoders.

* **UnexpecTED intent policy** is an auxiliary policy that enables the assistant to review the conversation so that can handle properly the unexpected user intents.

* **Memoization policy** compares the current conversation to the available stories in the training data. If there is a matching, the next action will be predicted with a confidence level of 1.

* **Augmented memoization policy** learns from stories in the training as Memoization policy does. The only difference is the forgetting mechanism that helps Augmented memoization find a match by reducing the conversation history.

On the other hand, **rule-based policies** are fixed behavior policies that tackle conversations and make predictions based on the scenarios you trained them. Also, they can be used in tandem with machine learning policies.


### Slide 8: Useful Features

Additionally, Rasa has some other useful features that can help to manage the conversation state and improve the bot performance. 

For example, **Tracker Store** keeps track of your conversation history as you interact with your bot, the conversation will be stored in the memory or in the SQL database.

Another feature that can be important for evaluating your bot is called **Markers**. **Markers** are kind of conditions that allow you to mark the points of interest in your dialog to evaluate your bot.

### Slide 9: Common Problems

However, some features in Rasa can cause some redundancy and confusion while improving the bot performance.

For example, the **entity extraction** feature is useful for filling forms or getting information from the user, but the feature is not designed to make if and else decisions to create forks in the dialog tree.

On the other hand, **intents** in Rasa are callable at all times and the **checkpoints** that are used to reduce the repetitivity can make the conversation flow hard to understand and make the bot miss everything.

### Slide 10: References

* [Rasa Dialog Trees without the Trees – ProAI.org](https://proai.org/2022/04/01/)

* [More blogs about Rasa chatbot development](https://rochdikhalid.substack.com/)

* [Rasa open source documentation](https://rasa.com/docs/rasa/)

* [Finite State Machines – Massachusetts Institute of Technology](http://web.mit.edu/6.111/www/f2017/handouts/L06.pdf)

* [Named Entity Recognition – Natural Language Processing of Semitic Languages](https://link.springer.com/chapter/10.1007/978-3-642-45358-8_7)

* [Markers in Rasa Open Source](https://rasa.com/blog/markers-in-rasa-open-source-3-0/)


