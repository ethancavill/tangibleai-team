# Organizing your Front-End Codebase in a Django Project

There are two main approaches of writing widely spreaded accross community: Back-End and Front-End. Let's consider them both:

![architectures somparison](exercises/server-architectures-compare.png)

Generally, server-first architecture is bad because javascript code snippets dropped in html template will make a code mess really quickly. For client-first architecture disadvantage comes when you want to use some common functionality for example form validation which can be done in a few lines of code in django, but takes much effort in javascript.

Here is where Hybrid architecture enters the game. The key concept is that in a hybrid architecture, rather than choosing between client-first or server-first at the project level, we choose it at the page level. 

Remember, in a hybrid application we'll have all of the the following types of pages:

    - Server-first Django pages with little-to-no JavaScript (pure Django pages are the simplest and most straightforward to set up since they have basically zero front-end code. In many applications these are far-and-away the most common)

    - Client-first JavaScript pages with little-to-no Django (where we want a more complex, responsive UI built using a JavaScript framework)
    
    - Everything in between (we will be covering this here in detail)

# A Crash Course in Modern JavaScript Tooling

In this part, we're going to cover the basics of the modern JavaScript toolchain that we'll need to build the hybrid application that we covered in previous section

### The modern JavaScript tool chain

![javascript toolchain](exercises/javascript-toolchain.jpg)

It's just three things!
Let's get into those in a little more depth.

### Package manager

The first part of the toolchain is the package manager. This is the thing that allows you to import and use packages that other people have built—basically pip but for JavaScript. There are two popular packages managers out there, Yarn, and npm. JavaScript people can have strong opinions about which one is best, but honestly they're both fine. Npm is more popular. Yarn is newer and does a few things NPM can't do.

### Bundler

The bundler's job is to take your code—the libraries you're using from the package manager plus any code that you've written—and, well bundle it up. Bundlers take code spread across a bunch of places and mash them together so you only have to drop small number of files on your site—making it more performant. The most popular one is webpack.

### The compiler (transpiler)

The compiler's job is take those and make them backwards compatible, so they work with the pesky devices—mostly browsers—that don't understand them yet. In this guide we will be using babel

![javascript toolchain](exercises/javascript-toolchain-general-image.png)

# Integrating a Modern JavaScript Pipeline into a Django Application

### How JavaScript and Django will integrate

Basically, we're going to create the JavaScript pipeline we covered in previous section as a standalone, separate environment inside our Django project.

Then to integrate with Django, the outputs (remember those bundle files?) will be static files that we'll drop into our templates wherever we need them. This will allow us to create anything from single-page apps, to reusable JavaScript libraries while maintaining the best of Django and JavaScript tooling.

![javascript toolchain](exercises/django-javascript-composition.jpg)

### Laying out your project

At a high level, it's suggested making your JavaScript project a subfolder in the root of your larger Django project. This guide uses a folder called ```./assets/``` for the front-end source files, though you could also use ```./front-end/```, ```./js/``` or whatever you want really.


├── manage.py
├── mysite
│   ├── __init__.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
├── myapp      <----- a normal Django app
│   ├── __init__.py
│   ├── models.py
│   ├── urls.py
│   └── views.py
├── assets     <----- our front-end project source
│   ├── javascript
│   └── styles
├── static     <----- our front-end project outputs (and other Django static files)
│   ├── css
│   ├── images
│   └── js  
└── templates  <----- our Django template files
    └── myapp

The main folders we'll be talking about for the purposes of this guide are ```assets``` and ```static```.

Then follow these steps:
https://www.saaspegasus.com/guides/modern-javascript-for-django-developers/integrating-javascript-pipeline/#setting-up-webpack-inside-your-django-project

### How to build a React application in a Django project

# Single-page-applications in a hybrid architecture

Some pages—for example, a login page—might be traditional Django templates and forms, while other pages might be almost 100% JavaScript. In this section we cover that second category of pages. Pages we dubbed client-first that are essentially single-page-applications embedded inside a Django project.

# Authentication

Because we're serving our pages directly from Django we can just use all of Django's built-in tooling to handle authentication and not rely on complex third-party authentication workflows.

That means we can just use the @login_required decorator or LoginRequiredMixin on the hybrid Django view serving our template and Django handles the rest. No wrestling with CORS or 3rd-party authentication frameworks is necessary.

# Passing data from the back end to the front end

In a hybrid architecture there are two mechanisms to send data to the front end:

    - Pass the data directly to the template, using Django's built-in templating system.
    - Provide the data via asynchronous APIs, using JavaScript and Django REST framework (DRF).
