# Git Workflow Example

This workflow assumes you've already set up your ssh keys and connected the user on your laptop to your username on gitlab:

```console
ssh-keygen
cat ~/.ssh/id_rsa.pub
# copy the public key text to your clipboard
# paste the public key into your GitLab profile
#   (preferences->ssh-keys->add new key)
```

You also should have previously identified yourself to your local git repository:

```console
git config --global user.name 'Hobson Lane'
git config --global user.email 'engineering@tangibleai.com'
```

The rest of this tutorial shows you a git workflow that will help keep you out of "git hades."
If you are contributing to a new project, such as the `tangibleai/machine-translation` project, you start by cloning the repo:

```console
# this will download a copy of the directory containing the code
git clone git@gitlab.com/tangibleai/machine-translation

# Change your working directory to the new directory you just downloaded
cd machine-translation
```

Then you can create a Python development environment to hold all the packages you will need to install.
You want to have the same exact libraries and packages that the other people on the project have.
Otherwise you may have errors when you try to run the python code in their project.

Most projects have a requirements.txt file or a setup.py and setup.cfg file that specify all the packages you need to install in that environment.
Here's how to do all that with conda:

```
# create a conda environment with python 3.7 and name it "nmt"
conda create -n nmt "python>3.7.5,<3.8"

# activate your shiny new environment
conda activate nmt

# install the package in editable mode within your conda environment
pip install -e .  
```

Now we need to create a branch (version) of all the code so you can edit it without interfering with other developers:

### Personal branch

```
# create a new branch based on master called "yourname"
git checkout master -b hobson
# push your branch up to gitlab (origin) and set the "-u"pstream branch to have the same name
git push -u origin yourname
```

Now we're finally ready to get into the proper git workflow that you'll use every day.
You can now edit anything you like and push your changes and it won't break other people's code.

### Small change

Let's say you want to add a requirement for torch to requirements.txt
You'd normally use Sublime Text or your favorite text editor to do this. But you can always use the `echo` command (bash's print command) to append things to a file, if you like:

```console
echo "torch" >> requirements.txt

# use the `git status` command to make sure the changes are what you intended
git status

# you can use the -am option to automatically add changes and specify a message
git commit -am "add torch to the bottom requirements.txt"

# now you can push your changes without specifying the remote name
#   (before you had to do `git push origin yourname`)
git push
```

### New code

Sometimes you want to create a new file or copy an existing one:

```console
# if you want to completely change the function of a script copy the script you want to edit
cp src/nmt/toy_problem.py src/nmt/train_questioner.py

# you can see that the new file isn't tracked yet with the git status command
git status

# if you added a new file you'll need to use git add
git add src/nmt/train_questioner.py

# it should now be green in git status
git status

# now you can commit and push
git commit -am "starting on the questioner trainer script"
git push

# Now visit gitlab.com/tangibleai/machine-translation
# You should see a button to create a merge request to merge your branch into master
# That will let everyone see and review what you've done
# They can then merge it into master and use your shiny new feature
firefox http://gitlab.com/tangibleai/machine-translation
```

### Solo

1. Edit a file in sublime or PyCharm
2. `[CTRL]-S` to save your changes
3. `git status` to see which files changed (optionally `git diff`) 
4. `git add` only if it's a new file
5. `git commit -am "describe what changed here (2-10 words)"`
6. `git status` to make sure everything you would like has been committed
7. `git push`
8. Create a merge request
9. Send a link to the merge request to someone else on the team asking them to review it
10. merge the code once someone has looked at the "diff" tab on the merge request and approved it.

## Teamwork

Once you are ready to work with a team on the same file or project, you need to work with the `main` branch.
To keep up to speed and avoid merge conflicts (that can waste hours of your day):

1. create a branch with your name e.g. `git checkout main -b vlad`
2. immediately create a merge request from your branch to the main branch on GitLab your merge request description can include what you plan to do or your sprint plan pasted into the text box
3. every time you start working do `git pull origin main` and resolve any merge conflicts locally on your laptop
4. every time you stop working on it (to have a break or work on something else), or every hour, or once you finish some small change that works in your local tests working (even after a small change) push your changes to gitlab, e.g.: `git push origin vlad`
5. every time you have tests passing on your local machine push your code up to your merge request and send a link to the merge request to cetin or I, asking us to review your changes and merge it to `main`
