### Try to find a better algorithm to calculate by delta

#### The task is to create better algorithm to calculate unique user ID's by date and delta.

Delta represent day, week and month. (1 , 7, 30)
For each day we want to to know how many daily, weekly, and monthly users created a message.

If you want to solve it in SQL. You can insert the `message.csv` to sqlite3 DB.


- Install the packages

```
pip install Faker
pip install pandas
pip install numpy
```


- Generate sample data, the command below will create a `messages.csv` file.

```
python3 create_data
```

- Create a better algorithm to find unique user ids by delta. There a sample calculation in the `calculate_by_delta.py`.
