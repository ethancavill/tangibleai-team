import numpy as np
import pandas as pd


def get_data():
    print("Generating dataframe!")

    # Query string to select data from Supabase with Django connection
    df = pd.read_csv("messages.csv")

    return df


def calculate_metrics(df, delta):
    print("Starting calculations!")

    # For each day leave one record
    df = df
    df["date"] = df["date"].apply(lambda x: np.datetime64(x))
    df.drop_duplicates(keep="first", inplace=True)

    start_date = np.datetime64(df["date"].min().date())
    stop_date = np.datetime64(df["date"].max().date())

    final_list = []
    current_date = start_date
    while current_date <= stop_date:
        date_range_start = current_date - np.timedelta64(delta, 'D')
        mask = (df["date"] > date_range_start) & (df["date"] <= current_date)
        new_df = df[mask]
        item = {
            "date": current_date,
            "active_user": new_df["user_id"].nunique(),
            "delta": delta,
        }
        print(item)
        final_list.append(
            # count of the users on that date with a 'delta' window
            item
        )
        current_date = current_date + np.timedelta64(1, 'D')

    final_df = pd.DataFrame.from_records(final_list)
    final_df.to_csv(f"calculations{delta}.csv")


if __name__ == "__main__":

    # Get base data for calculations
    df_ = get_data()

    delta_list = [1, 7, 30]

    for delta_ in delta_list:
        calculate_metrics(
            df=df_,
            delta=delta_,
        )
