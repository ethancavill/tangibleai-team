## for plotting
import matplotlib.pyplot as plt  #3.3.2
import seaborn as sns

from nlpia2_wikipedia import wikipedia as wiki

import spacy  #3.5.0
from spacy import displacy
import textacy  #0.12.0

import networkx as nx  # 3.0 (also pygraphviz==1.10)

from nlpia2.spacy_language_model import nlp
# from grounder.spacy_language_model import nlp
import pandas as pd

wiki.set_lang('en')
import matplotlib.pylab as plt
import networkx as nx

from textacy.extract import subject_verb_object_triples



def tag_sentence(sentence, tags='pos_ dep_'.split()):
    if isinstance(sentence, str):
        sentence = nlp(sentence)
    tokens = [t for t in sentence]
    if not tags or isinstance(tags, str):
        if tags in ('all', '*', '', None):
            tags = [
                label for label in dir(tokens[0]) 
                if (label.endswith('_') and not label[0] =='_') or label == label.strip('_')
                ]
        else:
            tags = tags.split()
    tags = ['text'] + list(tags)
    return pd.DataFrame(
        [[getattr(tok, tag) for tag in tags] for tok in tokens],
        columns=tags)


def spacy_extract_entities(docs, label=None):
    ents = []
    for i, d in enumerate(docs):
        for e in d.ents:
            if not label or e.label_ == label:
                ents += [[i, e.text, e.label]]
    return pd.DataFrame(ents, columns='sentence entity label'.split())


def textacy_extract_triples(sentences):
    """ Extract [subject verb object] triples (subject-predicate-object) using textacy"""
    triples = []
    for i, sent in enumerate(sentences):
        sent_triples = subject_verb_object_triples(sent)  
        for t in sent_triples:
            triples.append(t._asdict())

    return pd.DataFrame(triples)


if __name__ == '__main__':
    page = wiki.page('large language model')
    text = page.content[:1510]
    sentences = list(nlp(text).sents)
    sent = sentences[0]
    tag_sentence(sent)
    entities = spacy_extract_entities(sentences)
    triples = textacy_extract_triples(sentences)


    nodes = []
    relations = []  # iterate over the triples
    for triple in triples:  # extract the Subject and Object from triple
        node_subject = "_".join(map(str, triple.subject))
        node_object  = "_".join(map(str, triple.object))
        nodes.append(node_subject)
        nodes.append(node_object)  # extract the relation between S and O
        # add the attribute 'action' to the relation
        relation = "_".join(map(str, triple.verb))
        relations.append((node_subject,node_object,{'action':relation}))# remove duplicate nodes

    nodes = list(set(nodes))
    print(nodes)


    G = nx.DiGraph()
    G.add_nodes_from(nodes)
    G.add_edges_from(relations)

    edge_attribute = nx.get_edge_attributes(G, 'action')
    edges, weights = zip(*edge_attribute.items())# resize figure
    plt.rcParams["figure.figsize"] = [5, 2]
    plt.rcParams["figure.autolayout"] = True# set figure layout
    pos = nx.circular_layout(G)# draw graph
    nx.draw(G, pos, node_color='b', width=2, with_labels=True)# draw edge attributes
    nx.draw_networkx_edge_labels(G, pos,edge_attribute, label_pos=0.75 )
    plt.show()
