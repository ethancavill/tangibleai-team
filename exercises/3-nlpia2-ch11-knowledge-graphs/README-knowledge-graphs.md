# Knowledge Graphs

Chapter 11 is all about extracting information from natural language text and then automatically identifying useful tidbits of knowledge in the text and storing it in a knowledge graph.

### Exercise

1. Read this brief explanation of [knowledge graphs](https://web.stanford.edu/class/cs520/2020/notes/What_is_a_Knowledge_Graph.html)

2. From the paper, finish this graph in a yaml file (hint you should have 6 edges total:

```yaml
edges:
    - [Albert Einstein, Germany, born in]
    - [Albert Einstein, Theoretical Physicist, kind of]
```


3. plot your graph using `yaml_graphviz.py` or one of these:

- graphviz example by @Aransil: https://github.com/redransil/plotly-dirgraph
- Plotly example by Roland Smith: https://stackoverflow.com/a/59538246/623735
- Manim docs: https://docs.manim.community/en/stable/reference/manim.mobject.graph.Graph.html
- Ploty docs: https://plotly.com/python/network-graphs/

### References

* [Stanford 2021 Knowlege Graph Course](https://www.youtube.com/playlist?list=PLDhh0lALedc5paY4N3NRZ3j_ui9foL7Qc)
* [Stanford Knowlege Graph Course notes by Vinay](http://www.web.stanford.edu/~vinayc/kg/notes/How_Do_Users_Interact_With_a_Knowledge_Graph.html)
* recent [tutorial on Towards Data Science](https://12ft.io/proxy?&q=https%3A%2F%2Ftowardsdatascience.com%2Fnlp-with-python-knowledge-graph-12b93146a458)
* Knowledge Representation Learning python library with state of the art performance on academic benchmarks: [OpenKE Python package](https://github.com/thunlp/OpenKE)
