# Free eBook for NLPiA

## First Edition eBook

To get a free copy of Natural Language Processing in Action 1st Edition

1. browse to manning's listing of the 1st Edition here: [proai.org/nlpia-book](https://www.manning.com/books/natural-language-processing-in-action)
3. Click blue **[add to cart]** button
4. Click on the shopping cart [icon](https://www.manning.com/cart), second from the right at the top of the page
5. Look for the discount code box and enter `nlpaction6`
6. Click blue **[apply]** button and wait for the shopping cart price to be 0.00
7. Click the big blue **[checkout]** button
8. Get through the signup process
9. Add the PDF book to your Dropbox or download it to your PC so you can read it offline

## Second Edition

The second edition live book is available here: [proai.org/nlpia-book2](https://www.manning.com/books/natural-language-processing-in-action-second-edition?query=hobson%20lane)
