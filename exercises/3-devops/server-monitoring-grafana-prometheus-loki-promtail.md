# Monitoring with Grafana

[grafana+prometheus tutorial](
https://oastic.com/posts/how-to-monitor-an-ubuntu-server-with-grafana-and-prometheus/)

## Dependencies

```bash
sudo apt update
sudo apt install -y curl git wget make gcc software-properties-common
# headless web browser
sudo apt install -y w3m
sudo apt install -y grafana prometheus prometheus-node-exporter promtail loki
# alerting in grafana?
sudo apt install -y mimir  
```

Promtail is already enabled and started:

```bash
sudo systemctl status promtail
● promtail.service - Promtail service
     Loaded: loaded (/etc/systemd/system/promtail.service; enabled; vendor preset: enabled)
```

## rsyslog

Edit rsys log to reduce logstash data requirements per this [tutorial](https://www.thegeekdiary.com/understanding-the-etc-rsyslog-conf-file-for-configuring-system-logging/):

```bash
sudo nano /etc/rsyslog.d/50-default.conf
sudo nano /etc/rsyslog.conf
```

## loki


```bash
which loki
# /usr/bin/loki

sudo systemctl enable loki
sudo systemctl start loki
sudo systemctl status loki
# ● loki.service - Loki service
#      Loaded: loaded (/etc/systemd/system/loki.service; enabled; vendor preset: enabled)

loki -config.file /etc/loki/config.yml -version
# /usr/bin/loki -config.file /etc/loki/config.yml -version
#   loki, version HEAD-e0af1cc (branch: HEAD, revision: e0af1cc8a)

curl https://github.com/grafana/loki/tags | grep -B2 -A6 'e0af1cc' | egrep 'v[0-9]+[.][0-9]+[.][0-9]+'
# <a class="Link--muted" href="/grafana/loki/archive/refs/tags/v2.7.1.zip" rel="nofollow">

cat /etc/systemd/system/loki.service

cat /etc/loki/config.yml
```

Find more ideas for loki/config.yaml files on github:

`https://raw.githubusercontent.com/grafana/loki/v2.7.1/cmd/loki/loki-local-config.yaml`


## firewall

sudo ufw allow 3000
sudo ufw allow 3100
