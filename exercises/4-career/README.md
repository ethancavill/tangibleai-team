
Some resources to help you find a job you love:

## Preparing for interviews

* Senior level technical interviews: [How to find a good CTO](../../blog/CTO -- Interview Questions.md) 
* [Technical interview questions from an insurance company](ds-interview-questions-insurance-company.md)
* [Technical interview questions](ds-interview-questions.md)

## Job postings

* Indeed
* Linked In

## Networking

* Converences
* Webinars
* Meetup groups
* Slack for meetup groups


