import pandas as pd
from scipy.sparse import coo_matrix


def from_csv_to_organized_matrix(csvfile):
    messages_data = pd.read_csv(csvfile, usecols=["date", "user_id"])
    df = pd.DataFrame(messages_data.value_counts(["date", "user_id"]))
    return coo_matrix(df.sort_values(by=["date"]))


def from_csv_to_matrix(csvfile):
    messages_data = pd.read_csv(csvfile, usecols=["date", "user_id"])
    return coo_matrix(messages_data)



# messages_data = pd.read_csv('messages2.csv', usecols=["date", "user _id"])
# new_mess = messages_data.value_counts(["date", "user_id"])
# df = pd.DataFrame(new_mess)
# organized_df = df.sort_values(by=["date"])
# matrix_df = coo_matrix(organized_df)