# 2023 prosocial AI project ideas

- maitag (ruslan+john+alex?)
- legalbot in qary.ai (Mykhailo+vlad+cetin)
  - qary.ai/immigrate
- SO qary-search (john+alex)
  - find dataset of SO QA pairs
  - upload to search.qary.ai
  - add FAQ feature to qary-search
- finance qary-search (seb, john)
  - find quarterly reports dataset
  - find "labels" for quarterly reports
  - break up quarterly reports into hierarchical (graph) data structure
  - upload quarterly reports to qary-search
  - use pretrained RCQA model with financial reports
  - create embeddings of sections of quarterly reports
  - semantic search on paragraphs/sections of quarterly reports
  - summarizer on quarterly reports 
- Semantic search (seb)
  - dataset of QA pairs from tagged quotes.yml (Q=tag)
  - create FAQ about tangible AI 
  - python function to find semantic matches
    - TFIDF vector cosine distance
    - spacy vector cosine distance
    - BERT cosine distance
  - SCANN, Annoy, postgres vector search
  - index some SO articles
     or gitlab code snippets? numerai? LLM benchmarking? self-hosted MLFlow? **self-hosted Django tutorial for simplified MLFlow?**)
- faceofqary (vlad, seb) 
- qary (Ruslan)
  - fix yaml file load and convo manage
  - django ORM to store preprocessed yaml file
  - django cache to store joblib for 
- better graph viz (Jon Sundin)
  - plotly 
  - d3 from python
- mlops website (ruslan, vlad s, vlad z, or alex)
  - maitag
  - django tutorial
  - mlflow.org
  - wandb.com



## Seb
- javascript? typescript? svelte?
