>>> import spacy
>>> nlp = spacy.load('en_core_web_md')
>>> doc = nlp('I would like to learn math.')
>>> for tok in doc:
...     print(tok.text, tok.pos_, tok.tag_)
...
>>> table = [[tok.text, tok.pos_, tok.tag_] for tok in doc]
>>> table
[['I', 'PRON', 'PRP'],
 ['would', 'AUX', 'MD'],
 ['like', 'VERB', 'VB'],
 ['to', 'PART', 'TO'],
 ['learn', 'VERB', 'VB'],
 ['math', 'NOUN', 'NN'],
 ['.', 'PUNCT', '.']]
>>> df = pd.DataFrame([[tok.text, tok.pos_, tok.tag_] for tok in doc])
>>> df = pd.DataFrame([[tok.text, tok.pos_, tok.tag] for tok in doc])
>>> import pandas as pd
>>> df = pd.DataFrame([[tok.text, tok.pos_, tok.tag] for tok in doc])
>>> df
       0      1                     2
0      I   PRON  13656873538139661788
1  would    AUX  16235386156175103506
2   like   VERB  14200088355797579614
3     to   PART   5595707737748328492
4  learn   VERB  14200088355797579614
5   math   NOUN  15308085513773655218
6      .  PUNCT  12646065887601541794
>>> df = pd.DataFrame([[tok.text, tok.pos_, tok.tag_] for tok in doc])
>>> df
       0      1    2
0      I   PRON  PRP
1  would    AUX   MD
2   like   VERB   VB
3     to   PART   TO
4  learn   VERB   VB
5   math   NOUN   NN
6      .  PUNCT    .
>>> numdoc = nlp('I would like to learn math for numbers like one 2 thre.')
>>> df = pd.DataFrame([[tok.text, tok.pos_, tok.tag] for tok in doc])
>>> df = pd.DataFrame([[tok.text, tok.pos_, tok.tag] for tok in numdoc])
>>> df
          0      1                     2
0         I   PRON  13656873538139661788
1     would    AUX  16235386156175103506
2      like   VERB  14200088355797579614
3        to   PART   5595707737748328492
4     learn   VERB  14200088355797579614
5      math   NOUN  15308085513773655218
6       for    ADP   1292078113972184607
7   numbers   NOUN    783433942507015291
8      like    ADP   1292078113972184607
9       one    NUM   8427216679587749980
10        2    NUM   8427216679587749980
11     thre   NOUN    783433942507015291
12        .  PUNCT  12646065887601541794
>>> df = pd.DataFrame([[tok.text, tok.pos_, tok.tag_] for tok in numdoc])
>>> df
          0      1    2
0         I   PRON  PRP
1     would    AUX   MD
2      like   VERB   VB
3        to   PART   TO
4     learn   VERB   VB
5      math   NOUN   NN
6       for    ADP   IN
7   numbers   NOUN  NNS
8      like    ADP   IN
9       one    NUM   CD
10        2    NUM   CD
11     thre   NOUN  NNS
12        .  PUNCT    .
>>> df = pd.DataFrame([[tok.text, tok.pos_, tok.tag_, tok.vector[:3]] for tok in numdoc])
>>> df = pd.DataFrame([[tok.text, tok.pos_, tok.tag_] + list(tok.vector[:3]) for tok in numdoc])
>>> df
          0      1    2         3          4         5
0         I   PRON  PRP -1.860700   0.158040  -4.14250
1     would    AUX   MD -2.239600   8.262000  -5.38310
2      like   VERB   VB -2.333400  -1.369500  -1.13300
3        to   PART   TO -1.648700  17.344999 -10.54400
4     learn   VERB   VB  2.736400   0.922330   1.05080
5      math   NOUN   NN  3.999700  -0.117180   0.41022
6       for    ADP   IN -7.078100  -2.688800  -4.08680
7   numbers   NOUN  NNS  0.029404   2.635000  -1.25900
8      like    ADP   IN -2.333400  -1.369500  -1.13300
9       one    NUM   CD -4.999000   2.336200  -0.54398
10        2    NUM   CD -1.684200 -15.249000   2.33640
11     thre   NOUN  NNS  0.166250  -1.305900  -1.61880
12        .  PUNCT    . -0.076454  -4.689600  -4.04310
>>> thre = nlp('thre')
>>> three = nlp('three')
>>> thre = nlp('thre').vector
>>> three = nlp('three').vector
>>> v1, v2 = three, three
>>> len(v1)
300
>>> import numpy as np
>>> v1 = v1/np.linalg.norm(v1)
>>> np.linalg.norm(v2)
57.93529
>>> np.linalg.norm(v1)
0.99999994
>>> thre = nlp('thre').vector
>>> three = nlp('three').vector
>>> sum((thre - three)**2))**.5
>>> sum((thre - three)**2)**.5
58.18979595423394
>>> np.linalg.norm(thre - three)
58.189796
>>> sum(np.abs(thre - three))
819.9827576875687
>>> from np.linalg import norm
>>> from numpp.linalg import norm
>>> from numpy.linalg import norm
>>> norm(thre)
38.217415
>>> norm(three)
57.93529
>>> norm(four)
>>> norm(nlp('four').vector)
67.837814
>>> norm(nlp('four').vector - three)
16.186926
>>> three = three/norm(three)
>>> thre = thre/norm(thre)
>>> norm(thre)
1.0
>>> norm(three)
0.99999994
>>> thre.dot(three)
0.3231544
>>> v1, v2 = three, three
>>> sum(v1 * v2)
0.9999999427890387
>>> v1.dot(v2)
0.99999994
>>> v1, v2 = thre, three
>>> v1.dot(v2)
0.3231544
>>> sum(v1 * v2)
0.32315438353475656
>>> v3 = nlp('four').vector
>>> sum(v1 * v3)
15.895587933962815
>>> v4 = nlp('four').vector
>>> v3 = nlp('three').vector
>>> v3
array([ -6.6973  ,  -3.1992  ,   0.63277 ,   5.3472  ,   6.0676  ,
         4.4675  ,  -1.7362  ,   9.1727  ,   2.7156  ,  -2.4749  ,
         7.1254  ,   3.4499  ,  -7.2304  ,  -1.4356  ,   3.1522  ,
        -2.3383  ,  -2.383   ,  -2.862   ,  -1.3866  ,   1.2203  ,
        -3.0746  ,  -1.7556  ,   3.7698  ,  -1.5681  ,   2.4826  ,
         1.8821  ,  -6.0009  ,  -5.2085  ,   2.8828  ,   2.3942  ,
         6.4872  ,   0.01187 ,   3.4239  ,  -6.6595  ,  -0.19717 ,
        -2.9827  ,  -3.4608  ,   3.2521  ,   0.63971 ,  -1.0644  ,
         3.9843  ,  -2.2531  ,  -2.7441  ,   1.6595  ,   5.9449  ,
         1.0214  ,  -3.6482  ,   0.49195 ,  -3.7436  ,   4.1681  ,
        -1.1841  ,   2.9788  ,  -3.2168  ,  -4.1115  ,  -4.5157  ,
         0.58867 ,  -3.9783  ,   3.0272  ,   3.847   ,   0.68006 ,
        -4.1506  ,   4.6019  ,   0.42899 ,  -0.60856 ,   5.9434  ,
         1.9104  ,  -6.1653  ,  -1.0711  ,   3.6109  ,  -2.0691  ,
         2.7492  ,  -0.7403  ,  -0.89319 ,   0.028188,   1.0927  ,
         3.1523  ,   1.3255  ,  -1.0016  ,  -1.35    ,   1.0923  ,
        -6.2309  ,   3.3619  ,  -4.0485  ,   2.2876  ,   5.6899  ,
        -0.21715 ,  -3.4062  ,  -2.7865  ,  -4.4452  ,   3.5248  ,
        -3.69    ,   2.0347  ,  -0.45362 ,   1.9853  ,  -1.1154  ,
         0.15991 ,   4.3534  ,  -1.8144  ,  -0.36525 ,   4.7285  ,
         2.3037  ,  -0.66808 ,  -0.22348 ,   3.6576  ,   1.9928  ,
         1.5627  ,  -0.52675 ,  -0.73206 ,   2.4536  ,  -2.0795  ,
        -0.28998 ,   4.4784  ,   0.25684 ,  -3.078   ,  -2.9614  ,
         0.79384 ,  -1.2885  ,   1.8819  ,  -0.58235 ,  -0.41306 ,
        -3.3029  ,  -1.553   ,   0.38828 ,   4.2723  ,  -2.139   ,
        -6.3258  ,   1.2608  ,   0.62224 ,   6.4269  ,   1.8409  ,
        -2.7408  , -10.059   ,   1.3035  ,   1.0908  ,   0.17664 ,
         3.8928  ,  -0.6437  ,   3.0111  ,   1.3711  ,  -2.5785  ,
         3.8312  ,   2.1557  ,  -0.33671 ,  -0.23768 ,   1.1307  ,
         1.2433  ,  -2.5663  ,  -5.6909  ,   5.135   ,   2.4024  ,
         2.4005  ,   1.2484  ,  -2.9556  ,  -2.7868  ,  -4.6688  ,
        -5.0311  ,  -0.3849  ,   0.16758 ,  -1.3626  ,  -3.2072  ,
        -4.6106  ,  -1.1601  ,   0.46995 ,   1.7429  ,   2.8039  ,
         2.6818  ,   1.6548  ,  -2.8764  ,   5.3224  ,   4.5213  ,
        -2.3424  ,   2.665   ,   4.4088  ,   1.3476  ,   5.461   ,
         2.8436  ,  -0.42321 ,   0.14371 ,  -3.7461  ,  -1.5001  ,
        -0.68192 ,   1.1907  ,  -3.2255  ,   7.6674  ,  -0.70153 ,
         3.0323  ,  -1.5979  ,  -0.035024,  -2.0151  ,   2.466   ,
         1.2252  ,  -4.0953  ,   3.2099  ,   3.6749  ,   3.5713  ,
         5.2797  ,  -2.5133  ,  -0.22315 ,  -2.9026  ,   3.9081  ,
        -0.62085 ,  -9.6585  ,   5.8328  ,  -7.3492  ,   8.4119  ,
         1.1967  ,   4.5676  ,  -0.6803  ,   2.5275  ,   5.0799  ,
         1.7404  ,   6.0607  ,  -0.61582 ,   2.2851  ,   1.6895  ,
         1.4916  ,   6.301   ,  -2.1917  ,  -3.2385  ,  -0.72084 ,
        -2.3795  ,   2.9534  ,   0.58182 ,   1.5506  ,  -7.7409  ,
        -4.177   ,  -1.379   ,   4.3508  ,   4.6316  ,   0.94024 ,
         5.4469  ,  -4.7858  ,  -1.4603  ,   2.3049  ,   4.473   ,
        -2.6876  ,  -3.6415  ,   5.9718  ,  -4.1229  ,   2.7483  ,
         0.42933 ,   2.1701  ,  -1.6776  ,   6.0049  ,   0.47673 ,
         2.3091  ,  -3.4818  ,   3.492   ,   3.9912  ,   2.7527  ,
         0.06506 ,  -8.995   ,  -4.9711  ,   6.2962  ,  -1.7141  ,
         3.5587  ,   0.19986 ,  -2.3447  ,   1.6699  ,   2.68    ,
        -0.28636 ,   0.47707 ,   0.30731 ,  -0.19696 ,   0.27277 ,
        -4.961   ,   1.2442  ,   1.2235  ,   1.8167  ,   2.9254  ,
         1.9626  ,  -5.2218  ,  -1.6162  ,   0.75817 ,  -3.3139  ,
         0.943   ,   0.61069 ,  -1.651   ,   0.086722,   1.6069  ,
         3.4564  ,  -2.0419  ,   0.8151  ,   3.2509  ,  -0.47737 ,
        -2.4336  ,  -0.44532 ,   2.6741  ,  -3.567   ,  -2.7095  ,
        -1.8843  ,  -6.8029  ,  -0.092846,   0.81508 ,   0.18384 ,
         1.4274  ,   1.6682  ,  -1.4268  ,   0.63458 ,  -2.4551  ],
      dtype=float32)
>>> v3.dot(v4)
3848.2256
>>> v3.dot(v4) / norm(v3) / norm(v4)
0.97914153
>>> v1000 = nlp('thousand').vector
>>> v10 = nlp('ten').vector
>>> v10.dot(v3) / norm(v3) / norm(v10)
0.751786
>>> hist -o -p -f embedding_numwords.hist.md
