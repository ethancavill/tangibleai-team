# Degoogling

https://old.reddit.com/r/degoogle/comments/ov1y01/email/

https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/

## streaming

- restream.io
- twitter
- twitch
- youtube
- facebook

## email

Google
  - $3/mo
  - calendar

Tutanota
  - $1/mo EU (Germany) 1GB $3/mo for 10GB and sharing of entire calendars
  - calendar

Protonmail (Switzerland)
  - calendar


#### [Privacy-focused E-Mail](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/)
-   [ProtonMail](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#1-protonmail) 
-   [Mailfence](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#2-mailfence) 
-   [Tutanota](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#3-tutanota) 
-   [Librem Mail](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#4-librem-mail)
-   [Thexyz](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#5-thexyz) 
-   [CounterMail](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#6-countermail)
-   [Soverin](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#7-soverin)
-   [Zoho Mail](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#8-zoho-mail)
-   [RMail](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#9-rmail)
-   [Hushmail](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#10-hushmail)
-   [Kolab Now](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#11-kolab-now)
-   [Posteo](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#12-posteo)
-   [Mailbox.org](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#13-mailbox-org)
-   [ServerMX](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#14-servermx)
-   [Private-mail](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#15-private-mail)
-   [Runbox](https://vpnoverview.com/privacy/anonymous-browsing/secure-email-providers/#16-runbox)
## docs (and email?)

NextCloud Office $36-$65 EU/year

## streaming

smaller twitch: https://restream.io
