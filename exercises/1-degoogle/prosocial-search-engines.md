Independent truth and privacy-focused search engines:

```yaml
- [URL, wiki, index, advertisement, quality, country, org, privacy]
- [qwant.com, en.wikipedia.org/wiki/Qwant, low-Ad (only rank-1 position), high recall & precision, France, for-profit, Pertimm, GDPR]
- [metager.org, en.wikipedia.org/wiki/MetaGer, Ad-free, high recall & precision, Germany, nonprofit, University of Hanover+, GDPR]
- [Brave.com, 100% independent]
- [Duck.com, 10% independent?]
```
 
