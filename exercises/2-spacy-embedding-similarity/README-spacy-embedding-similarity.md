# SpaCy Embeddings to Check Quiz Answers

## Quiz Bots

Have you ever taken an online quiz or filled out a form in a web page where it doesn't seem to understand your answer?
Maybe you type the correct answer to a quiz but it gets marked wrong because you used a synonym or typed the answer slightly differently than the teacher.
Natural language processing can fix that by helping the machine checking your answers to understand what you typed.
That way it can know how *similar* the meaning of what you typed is to the correct answer.

## Embeddings

Embeddings are a vector representation of natural language text that represent the *meaning* of the text.
You can use them to check a student's answer in a quizbot.
Rather than checking to see that the student has typed the correct answer exactly as the teacher typed it, you can check to see that they typed something that *means* something similar to the correct answer.

There are many ways to measure the similarity between vectors, but cosine similarity is the best approach for high dimensional vectors like embeddings.
Here are [two videos](https://www.dropbox.com/sh/3p2tt55pqsisy7l/AAB4vwH4hV3S9pUO0n4kTZfGa?dl=0) of pair programming sessions where we create vectors for words and phrases.
Then we use those vectors to compute the cosine similarity between to give a score for the correctness of an answer in a quiz based on the *meaning* of the student's answer.

## Play with embeddings

```python
>>> import spacy
>>> spacy.cli.download('en_core_web_sm')
>>> nlp = spacy.load('en_core_web_sm')
>>> correct_ans = nlp('print')

>>> student_ans = nlp('builtin print function')
>>> for tok in student_ans:
...     print(tok, tok.pos_)
>>> student_ans.vector
array([-0.155465  , -0.58753973, -0.11355793,  0.08235567, -0.07204167,
        0.21114902,  0.7235916 ,  0.38948765,  0.15231477, -0.3974842 ,
        ...
        0.26862347], dtype=float32)
>>> for tok in student_ans:
...     print(tok, tok.pos_, tok.vector[:5])
>>> student_ans.similarity(nlp('print function'))
0.6845483996302318
>>> nlp('print').similarity(nlp('print function'))
0.7463880570759143
>>> nlp('print').similarity(nlp('display'))
0.7879685814468386
>>> nlp('print').vector
array([-1.5660217 , -1.1361804 , -1.1663558 , -0.3407822 ,  0.5616151 ,
       -0.47388417,  1.2775246 ,  1.8446023 , -0.95345694, -0.37282792,
        ...
        0.60389584], dtype=float32)
>>> v1 = [-1.5660217 , -1.1361804 , -1.1663558 , -0.3407822 ,  0.5616151 ,
...        -0.47388417,  1.2775246 ,  1.8446023 , -0.95345694, -0.37282792,
...         0.7751173 , -0.32200623, -0.6070449 , -0.1483557 , -0.44542783,
...         0.92784524, -0.776528  , -1.3944267 ,  1.025951  ,  0.74316436,
...        -0.00522596,  0.55682874,  0.49861687, -0.3716693 ,  0.24806273,
...         0.39486647,  0.23584081,  1.0896254 , -0.5945621 , -0.4464902 ,
...         0.4229667 ,  0.6245276 ,  0.15609558,  0.573798  , -0.14049126,
...        -0.88277066,  0.3928036 ,  0.7242466 ,  1.623523  , -0.11636972,
...        -0.7136339 ,  0.28534526, -0.9693686 ,  0.30749977, -0.31512064,
...        -0.4694891 , -1.0729264 ,  0.8807678 , -0.1333421 ,  0.00504507,
...        -0.5449052 ,  0.55783594,  0.33979204,  0.04485925,  1.2726938 ,
...        -1.0541344 ,  1.0422554 , -1.1310364 , -0.11759897, -0.6725779 ,
...        -0.77509564, -0.56700456,  0.67883694, -0.92441654,  1.2892967 ,
...         0.12334412, -0.9483359 , -0.620983  , -0.17135654, -0.03698611,
...         0.15330014,  1.1398951 ,  1.2794653 , -0.3177781 , -1.0224147 ,
...         0.36170778, -0.47926676, -0.10592497,  0.37480935, -0.9484963 ,
...        -0.25889194, -0.33805737, -1.0172635 ,  0.24735013, -0.06043237,
...         1.0113864 , -0.905974  , -0.64369416, -1.5932384 ,  1.6685317 ,
...         0.04579628, -0.52614164,  1.2203274 ,  1.4379053 ,  0.51145965,
...         0.60389584]
...
>>> import numpy as np
>>> np.array(v1) / np.linalg.norm(v1)
array([-0.20190539, -0.14648644, -0.15037692, -0.04393666,  0.07240839,
        ...
        0.0778596 ])
>>> np.array(v1).dot(v1) / np.linalg.norm(v1) / np.linalg.norm(v1)
1.0
>>> v2 = list(nlp('display').vector)
>>> np.array(v1).dot(v2) / np.linalg.norm(v1) / np.linalg.norm(v2)
0.7879685885596497
>>> hist -o -p -f use_spacy_to_check_quiz_answers.hist.ipy
```
