# intern-challenge-05 nessvectors

This week you'll learn about **nessvectors**.
Nessvectors are word embeddings or vectors, such as word2vec or GloVe, that have been simplified to ignore everything except the components or dimensions you care about.

This [package](https://gitlab.com/tangibleai/nessvec) will show you how.

### Get started

`pip install nessvec`
OR
Clone the repository and install it from the source code.

Play around with the examples in the `README.md`.
Find a couple titles of 2 news articles and find out their nessvectors.
You can examine the politicalness and scienceness and kindness and femaleness of the words in the article title.

### BONUS:
Create a yaml file with a list of lists. Each sublist should have 5 elements. The titles of your articles should be the first element (column) of you sublists. And the 4 scores for scienceness, femaleness, etc should be float values in the remaining sublist elements. Push your examples to gitlab and merge request them into the nessvec repo so we can use them as examples in the book.

### SUPERBONUS: help the other interns do all this and consolidate your contributions into a single yaml file.

## Resources

### Rochdi Substack

- Rochdi's [Substack Post 1: Getting Started with Rasa](https://rochdikhalid.substack.com/p/getting-started-with-your-first-rasa-bot?r=55xte&utm_campaign=post&utm_medium=web&utm_source=url)
- Rochdi's [Substack  Post 2: Closer Look at Rasa Components (stories, intents, rules)](https://rochdikhalid.substack.com/p/closer-look-at-rasa-components?r=55xte&utm_campaign=post&utm_medium=web)
- Rochdi's [Substack Post 3: Design Rasa NLU Training Data](https://rochdikhalid.substack.com/p/design-nlu-training-data?utm_source=url)

### Rochdi's qary+Rasa astrobot

- qary state management and intent recognition bug [Rochdi video on Tangible AI slack](https://tangibleai.slack.com/files/U02S05A1UAG/F033HAZF0R1/attempt.mp4)
- qary astro bug [Rochdi's slack video]()
- finite state machines [tutorial on gamedev.net](https://gamedev.net/tutorials/_/technical/general-programming/finite-state-machines-and-regular-expressions-r3176/)
- gitlab merge request link with [direct link to line in the source code](https://gitlab.com/rochdikhalid/blog/-/merge_requests/14/diffs#496813743289af9b8b551c659d97ad334d444af4_47_47)
- gitlab merge request ["diff" view 1](https://files.slack.com/files-pri/TV2RD62G7-F031UN1ASDC/diff.png)
- [proactive push messaging in Rasa](https://rasa.com/docs/rasa/reaching-out-to-user)
- [papers with code NER topic](https://paperswithcode.com/area/natural-language-processing/named-entity-recognition-ner)

### Hobson's Weekly Mob Programming on Manning's Twitch

- what's in a name with word vectors [manning twitch session 1](https://www.twitch.tv/videos/1285366722)
- word vectors for synonym substitution [manning twitch session 2](https://www.twitch.tv/videos/1300742628)
- word vector (GloVe) indexing with PyNNDescent[manning twitch session 3](https://www.twitch.tv/videos/1293313018)
