# `workon`

Everyday I use the `workon` command.
It's a bash script I created to automate some of the things I do whenever I start working on a project.
It helps me find the right directory and Python virtual environment for the new project.
It's a `bash` function that needs to be defined in your `.bashrc` script.
The `.bashrc` is the shell script that runs every time you launch an interactive bash shell (usually within a Terminal application).

To use my `workon` function you can put the file [`bash_functions.sh`](./bash_functions.sh) in your user directory and then run that bash script from within your `.bashrc`.

#### *`.bashrc`*
```bash
source $HOME/bin/bash_functions.sh
```
