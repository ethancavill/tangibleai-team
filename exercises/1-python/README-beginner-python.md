# CS 179

### Fun ways to learn Python

* [Fundamentals of Python textbook] (https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/index.html) - Runestone Academy books are interactive and free!
* [EdaBit] (https://edabit.com/tutorial/python) - Lessons and hints and interactive interpretters
* [Python Anywhere] (https://www.pythonanywhere.com/registration/register/beginner/) - you can even deploy a Python web application (website) here
* [Interactive Python Debugger] (https://pythontutor.com/python-debugger.html#) - A debugger visualizes your code as it processes it one line at a time

### Get Started

Modules   Syllabus   Discussions   Grades

I'm Hobson Lane. My goal is to help each one of you become a successful Python programmer. Welcome to CISC 179 - Python Programming!

You will be using the Canvas Learning Management System (LMS) to manage all your assignments, exercises, and discussions. You will need to complete one Module (chapter and assignment) per week.

To get started:

1. Download Kenneth Lambert's free e-book [Fundamentals of Python](https://www.dropbox.com/s/9u5jgy0ksidklvu/Fundamentals%20of%20Python%20First%20Programs%20-%202nd%20Ed.pdf?dl=1)
2. Read Chapter 1 of [Fundamentals of Python](https://www.dropbox.com/s/9u5jgy0ksidklvu/Fundamentals%20of%20Python%20First%20Programs%20-%202nd%20Ed.pdf?dl=0)
3. Optionally, sign up for the free Runstone Academy [interactive Python textbook](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/index.html)
4. Create a [GitLab.com](gitlab.com) account and head over to my git repository where I keep some useful Python learning resources (http://gitlab.com/tangibleai/community/cs179/).

Head over to the course Modules

Kenneth Lambert's free e-book [Fundamentals of Python]() contains everything you need to succeed in this course.

# Welcome to CISC 179: Python Programming: Spring 2023 - CISC 179 (21616 - online)
.
