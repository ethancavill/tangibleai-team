from sentence_transformers import SentenceTransformer
encoder = SentenceTransformer('all-MiniLM-L6-v2')
encoder.encode('Hello world!')
from sklearn.linear_model import LogisticRegression
model = LogisticRegression()
from pathlib import Path
import pandas as pd
with next(Path.cwd().glob('*.csv')).open() as f:
    df = pd.read_csv(f)
df = df[df.columns[:2]]
df = df.dropna()
df.head()
import numpy as np
X = np.array([list(encoder.encode(x)) for x in df['Utterance']])
y = df['Label']
X.shape
model = LogisticRegression(class_weight='balanced')
model.fit(X, y)
model.score(X, y)

y_pred = model.predict(X)
df_probas = pd.DataFrame(model.predict_proba(X), columns=model.classes_)
df_probas['y_pred'] = y_pred
df_probas = df_probas.groupby('y_pred').mean()

import seaborn as sns
from matplotlib import pyplot as plt

sns.heatmap(df_probas[:'y_pred'], annot=True)
plt.tight_layout()
plt.show()
