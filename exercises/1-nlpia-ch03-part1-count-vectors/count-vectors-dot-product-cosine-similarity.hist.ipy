>>> nlp = spacy.load("en_core_web_md")
>>> import spacy
>>> nlp = spacy.load("en_core_web_md")
>>> >>> sentence = ('It has also arisen in criminal justice, healthcare, and '
... ...     'hiring, compounding existing racial, economic, and gender biases.')
...
>>> sentence
'It has also arisen in criminal justice, healthcare, and hiring, compounding existing racial, economic, and gender biases.'
>>> >>> doc = nlp(sentence)
... >>> tokens = [token.text for token in doc]
... >>> tokens
...
['It',
 'has',
 'also',
 'arisen',
 'in',
 'criminal',
 'justice',
 ',',
 'healthcare',
 ',',
 'and',
 'hiring',
 ',',
 'compounding',
 'existing',
 'racial',
 ',',
 'economic',
 ',',
 'and',
 'gender',
 'biases',
 '.']
>>> >>> from collections import Counter
... >>> bag_of_words = Counter(tokens)
... >>> bag_of_words
...
Counter({',': 5,
         'and': 2,
         'It': 1,
         'has': 1,
         'also': 1,
         'arisen': 1,
         'in': 1,
         'criminal': 1,
         'justice': 1,
         'healthcare': 1,
         'hiring': 1,
         'compounding': 1,
         'existing': 1,
         'racial': 1,
         'economic': 1,
         'gender': 1,
         'biases': 1,
         '.': 1})
>>> bag_of_words
Counter({',': 5,
         'and': 2,
         'It': 1,
         'has': 1,
         'also': 1,
         'arisen': 1,
         'in': 1,
         'criminal': 1,
         'justice': 1,
         'healthcare': 1,
         'hiring': 1,
         'compounding': 1,
         'existing': 1,
         'racial': 1,
         'economic': 1,
         'gender': 1,
         'biases': 1,
         '.': 1})
>>> import pandas as pd
>>> pd.Series(bag_of_words)
It             1
has            1
also           1
arisen         1
in             1
criminal       1
justice        1
,              5
healthcare     1
and            2
hiring         1
compounding    1
existing       1
racial         1
economic       1
gender         1
biases         1
.              1
dtype: int64
>>> >>> bag_of_words.most_common(3)  # <2>
[(',', 5), ('and', 2), ('It', 1)]
>>> counts = pd.Series(dict(bag_of_words.most_common()))
>>> counts
,              5
and            2
It             1
has            1
also           1
arisen         1
in             1
criminal       1
justice        1
healthcare     1
hiring         1
compounding    1
existing       1
racial         1
economic       1
gender         1
biases         1
.              1
dtype: int64
>>> counts / counts.sum()
,              0.217391
and            0.086957
It             0.043478
has            0.043478
also           0.043478
arisen         0.043478
in             0.043478
criminal       0.043478
justice        0.043478
healthcare     0.043478
hiring         0.043478
compounding    0.043478
existing       0.043478
racial         0.043478
economic       0.043478
gender         0.043478
biases         0.043478
.              0.043478
dtype: float64
>>> >>> sentence = "Algorithmic bias has been cited in cases ranging from " \
... ...     "election outcomes to the spread of online hate speech."
... >>> tokens = [tok.text for tok in nlp(sentence)]
... >>> counts = Counter(tokens)
...
>>> >>> import requests
... >>> url = ('https://gitlab.com/tangibleai/nlpia2/'
... ...        '-/raw/main/src/nlpia2/ch03/bias_intro.txt')
... >>> response = requests.get(url)
...
>>> response
<Response [200]>
>>> bias_intro = response.content.decode()
>>> bias_intro
'Algorithmic bias describes systematic and repeatable errors in a computer system that create unfair outcomes, such as privileging one arbitrary group of users over others.\nBias can emerge due to many factors, including but not limited to the design of the algorithm or the unintended or unanticipated use or decisions relating to the way data is coded, collected, selected or used to train the algorithm.\nAlgorithmic bias is found across platforms, including but not limited to search engine results and social media platforms, and can have impacts ranging from inadvertent privacy violations to reinforcing social biases of race, gender, sexuality, and ethnicity.\nThe study of algorithmic bias is most concerned with algorithms that reflect "systematic and unfair" discrimination.\nThis bias has only recently been addressed in legal frameworks, such as the 2018 European Union\'s General Data Protection Regulation.\nMore comprehensive regulation is needed as emerging technologies become increasingly advanced and opaque.\n\nAs algorithms expand their ability to organize society, politics, institutions, and behavior, sociologists have become concerned with the ways in which unanticipated output and manipulation of data can impact the physical world.\nBecause algorithms are often considered to be neutral and unbiased, they can inaccurately project greater authority than human expertise, and in some cases, reliance on algorithms can displace human responsibility for their outcomes.\nBias can enter into algorithmic systems as a result of pre-existing cultural, social, or institutional expectations; because of technical limitations of their design; or by being used in unanticipated contexts or by audiences who are not considered in the software\'s initial design.\n\nAlgorithmic bias has been cited in cases ranging from election outcomes to the spread of online hate speech.\nIt has also arisen in criminal justice, healthcare, and hiring, compounding existing racial, economic, and gender biases.\nThe relative inability of facial recognition technology to accurately identify darker-skinned faces has been linked to multiple wrongful arrests of men of color, an issue stemming from imbalanced datasets.\nProblems in understanding, researching, and discovering algorithmic bias persist due to the proprietary nature of algorithms, which are typically treated as trade secrets.\nEven when full transparency is provided, the complexity of certain algorithms poses a barrier to understanding their functioning.\nFurthermore, algorithms may change, or respond to input or output in ways that cannot be anticipated or easily reproduced for analysis.\nIn many cases, even within a single website or application, there is no single "algorithm" to examine, but a network of many interrelated programs and data inputs, even between users of the same service.\n'
>>> docs = [nlp(s) for s in bias_intro.split_lines() if s.strip()]
>>> docs = [nlp(s) for s in bias_intro.split('\n') if s.strip()]
>>> docs
[Algorithmic bias describes systematic and repeatable errors in a computer system that create unfair outcomes, such as privileging one arbitrary group of users over others.,
 Bias can emerge due to many factors, including but not limited to the design of the algorithm or the unintended or unanticipated use or decisions relating to the way data is coded, collected, selected or used to train the algorithm.,
 Algorithmic bias is found across platforms, including but not limited to search engine results and social media platforms, and can have impacts ranging from inadvertent privacy violations to reinforcing social biases of race, gender, sexuality, and ethnicity.,
 The study of algorithmic bias is most concerned with algorithms that reflect "systematic and unfair" discrimination.,
 This bias has only recently been addressed in legal frameworks, such as the 2018 European Union's General Data Protection Regulation.,
 More comprehensive regulation is needed as emerging technologies become increasingly advanced and opaque.,
 As algorithms expand their ability to organize society, politics, institutions, and behavior, sociologists have become concerned with the ways in which unanticipated output and manipulation of data can impact the physical world.,
 Because algorithms are often considered to be neutral and unbiased, they can inaccurately project greater authority than human expertise, and in some cases, reliance on algorithms can displace human responsibility for their outcomes.,
 Bias can enter into algorithmic systems as a result of pre-existing cultural, social, or institutional expectations; because of technical limitations of their design; or by being used in unanticipated contexts or by audiences who are not considered in the software's initial design.,
 Algorithmic bias has been cited in cases ranging from election outcomes to the spread of online hate speech.,
 It has also arisen in criminal justice, healthcare, and hiring, compounding existing racial, economic, and gender biases.,
 The relative inability of facial recognition technology to accurately identify darker-skinned faces has been linked to multiple wrongful arrests of men of color, an issue stemming from imbalanced datasets.,
 Problems in understanding, researching, and discovering algorithmic bias persist due to the proprietary nature of algorithms, which are typically treated as trade secrets.,
 Even when full transparency is provided, the complexity of certain algorithms poses a barrier to understanding their functioning.,
 Furthermore, algorithms may change, or respond to input or output in ways that cannot be anticipated or easily reproduced for analysis.,
 In many cases, even within a single website or application, there is no single "algorithm" to examine, but a network of many interrelated programs and data inputs, even between users of the same service.]
>>> 
... >>> counts = []
... >>> for doc in docs:
... ...     counts.append(Counter([t.text.lower() for t in doc]))  # <2>
... >>> df = pd.DataFrame(counts)
... >>> df = df.fillna(0).astype(int)  # <3>
... >>> df.head()
...
   algorithmic  bias  describes  systematic  and  repeatable  errors  in  a  ...  no  examine  network  interrelated  programs  inputs  between  same  service
0            1     1          1           1    1           1       1   1  1  ...   0        0        0             0         0       0        0     0        0
1            0     1          0           0    0           0       0   0  0  ...   0        0        0             0         0       0        0     0        0
2            1     1          0           0    3           0       0   0  0  ...   0        0        0             0         0       0        0     0        0
3            1     1          0           1    1           0       0   0  0  ...   0        0        0             0         0       0        0     0        0
4            0     1          0           0    0           0       0   1  0  ...   0        0        0             0         0       0        0     0        0

[5 rows x 246 columns]
>>> for i in range(len(df)): ((df.iloc[0] - df.iloc[i])**2).sum()**.5
>>> dists = []
... for i in range(len(df)):
...     dist = ((df.iloc[0] - df.iloc[i])**2).sum()**.5
...     dists.append(dist)
...
>>> dists
[0.0,
 10.535653752852738,
 8.831760866327848,
 5.656854249492381,
 6.164414002968977,
 5.916079783099616,
 7.745966692414834,
 7.874007874011811,
 8.660254037844386,
 5.830951894845301,
 7.348469228349534,
 7.615773105863908,
 6.324555320336758,
 6.244997998398397,
 7.0710678118654755,
 8.246211251235321]
>>> df.shape
(16, 246)
>>> cosdists = []
... for i in range(len(df)):
...     cosdist = df.iloc[0].dot(df.iloc[i]) / np.linalg.norm(df.iloc[0]) / np.linalg.norm(df.iloc[i])
...     cosdists.append(dist)
...
>>> import numpy as np
>>> cosdists = []
... for i in range(len(df)):
...     cosdist = df.iloc[0].dot(df.iloc[i]) / np.linalg.norm(df.iloc[0]) / np.linalg.norm(df.iloc[i])
...     cosdists.append(dist)
...
>>> dotprods = []
... for i in range(len(df)):
...     dotprod = df.iloc[0].dot(df.iloc[i])
...     dotprods.append(dist)
...
>>> dotprods
[8.246211251235321,
 8.246211251235321,
 8.246211251235321,
 8.246211251235321,
 8.246211251235321,
 8.246211251235321,
 8.246211251235321,
 8.246211251235321,
 8.246211251235321,
 8.246211251235321,
 8.246211251235321,
 8.246211251235321,
 8.246211251235321,
 8.246211251235321,
 8.246211251235321,
 8.246211251235321]
>>> dotprods = []
... for i in range(len(df)):
...     dotprod = df.iloc[0].dot(df.iloc[i])
...     dotprods.append(dotprod)
...
>>> cosdists = []
... for i in range(len(df)):
...     cosdist = df.iloc[0].dot(df.iloc[i]) / np.linalg.norm(df.iloc[0]) / np.linalg.norm(df.iloc[i])
...     cosdists.append(cosdist)
...
>>> dotprods
[27, 6, 12, 8, 6, 3, 10, 8, 12, 6, 9, 5, 10, 4, 5, 12]
>>> df
    algorithmic  bias  describes  systematic  and  repeatable  errors  in  a  computer  ...  there  no  examine  network  interrelated  programs  inputs  between  same  service
0             1     1          1           1    1           1       1   1  1         1  ...      0   0        0        0             0         0       0        0     0        0
1             0     1          0           0    0           0       0   0  0         0  ...      0   0        0        0             0         0       0        0     0        0
2             1     1          0           0    3           0       0   0  0         0  ...      0   0        0        0             0         0       0        0     0        0
3             1     1          0           1    1           0       0   0  0         0  ...      0   0        0        0             0         0       0        0     0        0
4             0     1          0           0    0           0       0   1  0         0  ...      0   0        0        0             0         0       0        0     0        0
5             0     0          0           0    1           0       0   0  0         0  ...      0   0        0        0             0         0       0        0     0        0
6             0     0          0           0    2           0       0   1  0         0  ...      0   0        0        0             0         0       0        0     0        0
7             0     0          0           0    2           0       0   1  0         0  ...      0   0        0        0             0         0       0        0     0        0
8             1     1          0           0    0           0       0   2  1         0  ...      0   0        0        0             0         0       0        0     0        0
9             1     1          0           0    0           0       0   1  0         0  ...      0   0        0        0             0         0       0        0     0        0
10            0     0          0           0    2           0       0   1  0         0  ...      0   0        0        0             0         0       0        0     0        0
11            0     0          0           0    0           0       0   0  0         0  ...      0   0        0        0             0         0       0        0     0        0
12            1     1          0           0    1           0       0   1  0         0  ...      0   0        0        0             0         0       0        0     0        0
13            0     0          0           0    0           0       0   0  1         0  ...      0   0        0        0             0         0       0        0     0        0
14            0     0          0           0    0           0       0   1  0         0  ...      0   0        0        0             0         0       0        0     0        0
15            0     0          0           0    1           0       0   1  2         0  ...      1   1        1        1             1         1       1        1     1        1

[16 rows x 246 columns]
>>> df.head()
   algorithmic  bias  describes  systematic  and  repeatable  errors  in  a  computer  ...  there  no  examine  network  interrelated  programs  inputs  between  same  service
0            1     1          1           1    1           1       1   1  1         1  ...      0   0        0        0             0         0       0        0     0        0
1            0     1          0           0    0           0       0   0  0         0  ...      0   0        0        0             0         0       0        0     0        0
2            1     1          0           0    3           0       0   0  0         0  ...      0   0        0        0             0         0       0        0     0        0
3            1     1          0           1    1           0       0   0  0         0  ...      0   0        0        0             0         0       0        0     0        0
4            0     1          0           0    0           0       0   1  0         0  ...      0   0        0        0             0         0       0        0     0        0

[5 rows x 246 columns]
>>> clear
>>> df.head()
   algorithmic  bias  describes  systematic  and  repeatable  errors  in  a  computer  ...  there  no  examine  network  interrelated  programs  inputs  between  same  service
0            1     1          1           1    1           1       1   1  1         1  ...      0   0        0        0             0         0       0        0     0        0
1            0     1          0           0    0           0       0   0  0         0  ...      0   0        0        0             0         0       0        0     0        0
2            1     1          0           0    3           0       0   0  0         0  ...      0   0        0        0             0         0       0        0     0        0
3            1     1          0           1    1           0       0   0  0         0  ...      0   0        0        0             0         0       0        0     0        0
4            0     1          0           0    0           0       0   1  0         0  ...      0   0        0        0             0         0       0        0     0        0

[5 rows x 246 columns]
>>> dotprods[:5]
[27, 6, 12, 8, 6]
>>> dotprods
[27, 6, 12, 8, 6, 3, 10, 8, 12, 6, 9, 5, 10, 4, 5, 12]
>>> cosdists = []
... for i in range(len(df)):
...     cosdist = df.iloc[0].dot(df.iloc[i]) / np.linalg.norm(df.iloc[0]) / np.linalg.norm(df.iloc[i])
...     cosdists.append(cosdist)
...
>>> df.head()
   algorithmic  bias  describes  systematic  and  repeatable  errors  in  a  computer  ...  there  no  examine  network  interrelated  programs  inputs  between  same  service
0            1     1          1           1    1           1       1   1  1         1  ...      0   0        0        0             0         0       0        0     0        0
1            0     1          0           0    0           0       0   0  0         0  ...      0   0        0        0             0         0       0        0     0        0
2            1     1          0           0    3           0       0   0  0         0  ...      0   0        0        0             0         0       0        0     0        0
3            1     1          0           1    1           0       0   0  0         0  ...      0   0        0        0             0         0       0        0     0        0
4            0     1          0           0    0           0       0   1  0         0  ...      0   0        0        0             0         0       0        0     0        0

[5 rows x 246 columns]
>>> cosdists[:5]
[1.0,
 0.11785113019775792,
 0.2666666666666666,
 0.33596842045264647,
 0.2407717061715384]
>>> mkdir /home/hobs/code/tangibleai/team/exercises/1-nlpia-ch3-count-vectors
>>> hist -o -p -f /home/hobs/code/tangibleai/team/exercises/1-nlpia-ch3-count-vectors/count-vectors-dot-product-cosine-similarity.hist.ipy
