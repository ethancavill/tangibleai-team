# Week 2 Exercise - Make out Like a Bandit

This week you're going to try your hand at a simplified version of an unsolvable academic statistics problem -- the "One-Armed Bandit" problem. 
You want to predict when and if you should place a "bet" on some random process like a Casino Slot machine. 
In academic literature this is called the one-armed bandit problem, there is also a variation called the "Multi-Armed Bandit" problem. 

This one-armed bandit that you are betting against automatically spins the wheels every 5 minutes for 2 years.
I have generated an array of 1M random numbers chosen from 2 different Gaussian distributions, switching once at a random moment in time within the 1M random samples.

## Example one-armed bandit array

For example, I might have generated the array like this:

```python
>>> import numpy as np
>>> x = np.round((3 * np.random.randn(500_000) + 4), 2)
>>> x = np.round(np.append(x, 5 + np.random.randn(500_000) - 6), 2)
```

This example array would be for a One-Armed Bandit that would spend the first year of the game paying out an average of average of $4.00 +/- $3.00 (1-sigma).
Halfway through the 1M samples it would switch to an average payout of -$5.00 (withdrawal from your account of $5) +/- $6.00 1-sigma.

## Example solutions

Before you start playing the game you can compute 10 numbers from this dataset to probe it, ask questions about the array in advance.

For example 3 of your 10 "questions" about the data might be:

1. The length of the array using the built-in `len()` function:

```python
>>> len(x)
1000000
```

2. The minimum value in the array using the `min()` function in numpy:

```python
>>> x[0]
7.64
```

3. The first value in the array using the indexing square brackets:

```python
>>> np.min(x)
-12.31
```

Your goal is to dream up 10 good "questions" to ask using python code that takes the array `x` as input and returns a single number as an answer.

## Submitting your answers

Download the array of payoffs in the x variable at this csv file: 
Submit your list of questions as python code (python functions or expressions) that could be run on the `x` variable to compute 10 numbers.
You can run the same code multiple times if you like. 
You can slice or index the array, any way you like as you compute your numbers.


Your goal is to use statistics about the dataset to decide when to step into the room with machine and start accepting the deposits or withdrawals from your payment card.
You are not allowed to ever see how much you won until the end of the two years.



If you had access to the statistics of this dataset could you decide in advance when to start placing bets and when to stop?  
Your answer to this assignment is 10 Python expressions that compute a single value from the variable, x, carrying a numpy array of length 1M with the reward values at each 5 minute time interval over the next 2 years (1M opportunities to place a bet).

The array is stored in the following CSV, but you are only allowed to download the dataset once and run your 10 lines of python code on the numpy array

As a stretch goal choose 2 integers between 0 and 1M that maximize your "profit".

Here's a simplified version of the problem. 

There is a dataset 



## 
