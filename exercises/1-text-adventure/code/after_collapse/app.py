import sys


# role playing

# 6 functions to represent the 6 "rooms"


def medicine():
    print("You fail to find medicine and your friend dies while you watch.")
    goodbye()


def cont():
    pass


def rest():
    pass


def goodbye():
    sys.exit("Goodbye")


def east():
    next_choice = input("To the east there is a river you can follow into the mountains. You don't "
                        "drink the water in the stream until you clear the city. The mountains are hot "
                        "however and you can't travel during the middle of the day. While hiking over"
                        " some rocks one of your party gets bit by a rattlesnake. You are days out "
                        "from the closest city and the leg that was bitten has already started to swell."
                        " You can wait a few days to see if they heal or continue on without them. Rest,"
                        " Continue, Exit")
    return next_choice


def west():
    print("To the west is the ocean. You and your party grab one of the boats that"
          " are vacant and set out to sea. You arrive on a sparsely populated island."
          " There are only a handful of police on the island and few people have weapons."
          " The desalination plant is still working. While power is only partially working "
          "the local population has agreed to use the electric for keeping the island "
          "supplied with drinking water. They are somewhat short on food supplies,"
          " they are making good use of the ocean. You are welcomed by locals and "
          "guided to now mostly empty hotels.")


def north():
    next_choice = input('To the north there is a marshy bog as you travel along the coastline. You are'
                   ' able to stop to fish and you can follow the estuaries to fresh water. The water is'
                   ' polluted but it is water. One of your party takes ill after drinking the water.'
                   ' Do you travel inland to look for medicine in a city or rest a few days to see if'
                   ' they recover: Medicine, Rest, Exit ')
    return next_choice

def south():
    print("To the south, you find only desert. There is no water. There is little shelter. You "
          "and your party die in the desert.")
    goodbye()


def main():
    while True:
        choice = input("The government collapsed a few months ago. In the chaos that followed"
                       " various armed groups claimed various resources. The police had the most weapons,"
                       "organization, and men. They easily over took the electric plant and have shut off "
                       "electricity to the parts of the city who have contested their control of the grocery stores."
                       "Many people initially took what supplies they could before the police established "
                       "control and those are running low but a greater problem looms. Some force that formed"
                       " to the north has stopped the water flow to the city. With supplies running low and no "
                       "water coming to the city, you are left with only the choice to leave. What direction "
                       "would you like to go in? North, East, South, West, Exit: ")

        choices = {"South": south, "West": west, "North": north, "East": east, "Exit": goodbye}
        next_choices = {"Medicine": medicine, "Rest": rest, 'Continue': cont}

        choice = choice.capitalize()
        while choice not in choices:
            choice = input("That wasn't an option, please choose: North, East, South, West, or Exit ")
        choices[choice]()


if __name__ == '__main__':
    main()
